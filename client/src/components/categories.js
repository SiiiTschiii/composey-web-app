import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import React from "react";
import './categories.css';

export function Categories(props) {

    function getPlaylistTitle() {

        if (props.playlists === undefined) {
            return ''
        } else if (props.playlists.length < props.currentPlaylist+1) {
            return ''
        } else {
            return props.playlists[props.currentPlaylist].name
        }
    }

    function handleSelect(e) {
        // TODO have no idea why e is a string

        let idx = parseInt(e)

        if (idx !== props.currentPlaylist) {
            props.handlePlaylistChange(idx)
        }

    }

    return (

        <DropdownButton variant='composey'
                        title={getPlaylistTitle()}
                        onSelect={handleSelect}
        >
            {props.playlists.map((playlist, index) =>
                <Dropdown.Item key={index} eventKey={index}>
                    {playlist.name}
                </Dropdown.Item>
            )}
        </DropdownButton>

    );
}
