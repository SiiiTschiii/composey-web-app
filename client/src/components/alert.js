import Container from "react-bootstrap/Container";
import Alert from "react-bootstrap/Alert";
import React from "react";

export function withAlert(WrappedComponent) {
    // ...and returns another component...

    return function(props) {
        return (
            <Container style={{ "padding-right": "0", "padding-left": "0"}}>
                {props.error && <Alert variant={'warning'}> {props.error}</Alert>}
                <WrappedComponent {...props} />
            </Container>
        )
    }
};
