import {Slider, Rail, Handles, Tracks} from 'react-compound-slider'
import {SliderRail, Handle, Track} from './components/slider'
import './settings.css';

import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import {Link} from "react-router-dom"; // example render components - source below


const sliderStyle = {
    position: 'relative',
    width: '100%',
    touchAction: 'none',
}

const domain = [0, 100];

export function Settings(props) {

    return (
        <Container>
            <Row>
                <Col xs={12} className='Top'>
                    <h3>Settings</h3>
                </Col>
            </Row>

            <Row>
                <MessageComposition weights={props.parameters}
                                    updateWeight={props.handleMsgCompParameterChange}
                />
            </Row>
            <Row>
                <Col xs={12} className='Top'>
                    <Link to="/">
                        <Card>
                            <Card.Body>
                                <Row className="d-flex justify-content-end">
                                    <Button variant="dark">Done</Button>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Link>
                </Col>
            </Row>

        </Container>
    );
}


function MessageComposition(props) {

    return (

        <Col xs={12}>
            <Card>
                <Card.Header>Message Composition Algorithm</Card.Header>
                <Card.Body>
                    <Card.Title>Similarity</Card.Title>
                    <WeightSlider name={"weightSimilarity"} value={props.weights.weightSimilarity}
                                  updateWeight={props.updateWeight}/>
                </Card.Body>
                <Card.Body>
                    <Card.Title>Few Parts</Card.Title>
                    <WeightSlider name={"weightNumParts"} value={props.weights.weightNumParts}
                                  updateWeight={props.updateWeight}/>
                </Card.Body>
                <Card.Body>
                    <Card.Title>Balanced Parts</Card.Title>
                    <WeightSlider name={"weightBalanced"} value={props.weights.weightBalanced}
                                  updateWeight={props.updateWeight}/>
                </Card.Body>
                <Card.Body>
                    <Card.Title>No Splits</Card.Title>
                    <WeightSlider name={"weightSplits"} value={props.weights.weightSplits}
                                  updateWeight={props.updateWeight}/>
                </Card.Body>
            </Card>
        </Col>

    );
}

class WeightSlider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            values: [props.value],
        };
    }

    onChange = values => {
        this.props.updateWeight(this.props.name, values[0]);
    };

    render() {
        const {
            state: {values},
        } = this;

        return (
            <Slider
                mode={1}
                step={1}
                domain={domain}
                rootStyle={sliderStyle}
                onUpdate={this.onUpdate}
                onChange={this.onChange}
                values={values}
            >
                <Rail>
                    {({getRailProps}) => <SliderRail getRailProps={getRailProps}/>}
                </Rail>
                <Handles>
                    {({handles, getHandleProps}) => (
                        <div className="slider-handles">
                            {handles.map(handle => (
                                <Handle
                                    key={handle.id}
                                    handle={handle}
                                    domain={domain}
                                    getHandleProps={getHandleProps}
                                />
                            ))}
                        </div>
                    )}
                </Handles>
                <Tracks right={false}>
                    {({tracks, getTrackProps}) => (
                        <div className="slider-tracks">
                            {tracks.map(({id, source, target}) => (
                                <Track
                                    key={id}
                                    source={source}
                                    target={target}
                                    getTrackProps={getTrackProps}
                                />
                            ))}
                        </div>
                    )}
                </Tracks>
            </Slider>
        )
    }
}