import React from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import ReactPlayer from 'react-player'
import Image from 'react-bootstrap/Image';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Modal from 'react-bootstrap/Modal';
import CopyToClipboard from 'react-copy-to-clipboard';
import {Link} from 'react-router-dom';
import {IoMdMore} from 'react-icons/io';

import logo from '../../images/logo.svg';
import './home.css';
// for AWS Signature 4 authentication with S3 streams
import {signV4headers} from '../../services/auth'
import {Categories} from "../../components/categories";

const S3 = require('./aws-s3-config.json');

function TopBar(props) {
    return (

        <Row className='TopBar'>
            <Col xs={12} className='ReducedPadding'>
                <div className="d-flex justify-content-between">
                    <Logo/>

                    <Categories playlists={props.playlists}
                                currentPlaylist={props.currentPlaylist}
                                handlePlaylistChange={props.handlePlaylistChange}
                    />

                    <Link to="/settings">
                        <Button type="button" variant="dark">
                            {/*<span className="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>*/}
                            {/*<h3> Lets go for a <FaBeer />? </h3>*/}
                            <IoMdMore />
                        </Button>
                    </Link>
                </div>
            </Col>
        </Row>
    )
}

function Logo() {
    return (
        <Image className='Logo' src={logo} rounded fluid/>
    )
}

class QueryForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({value: e.target.value});
    }

    handleSubmit() {
        //Propagate event to state owner
        this.props.onQueryFormSubmit(this.state.value, true);
        this.setState({value: ''});
    }

    render() {
        return (
            <Row className='QueryForm'>
                <Col xs={12}  className='ReducedPadding'>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Your message.."
                            aria-label="Type in your message"
                            aria-describedby="basic-addon2"
                            value={this.state.value}
                            onChange={this.handleChange}
                            // Triggers submit when enter is pressed.
                            onKeyPress={event => {
                                if (event.key === "Enter") {
                                    this.handleSubmit();
                                }
                            }}
                        />
                        <InputGroup.Append>
                            <Button
                                variant="dark"
                                onClick={this.handleSubmit}
                            >Compose</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Row>

        )
    }
}

function Video(props) {

    function makeKey(scene, index) {
        return scene.src + scene.start + scene.end + scene.type + index + props.candidateNumber
    }

    return (
        <div>
            {props.media.map((scene, index) =>
                <VideoPlayer key={makeKey(scene, index)} onEnd={props.onEnd} media={scene} index={index} s3={props.s3}/>
            )}
        </div>
    )
}


function VideoPlayer(props) {

    function isYoutube(type) {
        return (type === "youtube")
    }

    function partialVideo(start, end) {
        return !(start === end)
    }

    // choose the right player type
    if (isYoutube(props.media.type)) {

        // play youtube video from specified start- to endtime.
        return (
            <PlayerYouTube {...props}/>
        )

    } else if (partialVideo(props.media.start, props.media.end)) {

        // HLS video streaming from specified start- to endtime.
        return (
            <PlayerHLS {...props}/>
        )

    } else {

        // Play entire video file (e.g. mp4)
        return (
            <PlayerVideoFile {...props}/>
        )
    }

}

class PlayerHLS extends React.Component {

    constructor(props) {
        super(props);

        this.player = React.createRef();
    }

    onReady = () => {
        //console.log('onReady');
    }

    onStart = () => {
        //console.log('onStart');
    }

    onPlay = () => {
        this.player.seekTo(this.props.media.start / 1000);
        //console.log('onPlay');
    }

    onDuration = (duration) => {
        //console.log('onDuration', duration);
    }

    onProgress = state => {
        //console.log('onProgress', state);

        if (state.playedSeconds >= (this.props.media.end / 1000)) {
            //console.log('onProgress above end', state);

            //reset scene to start
            this.player.seekTo(this.props.media.start / 1000, 'fraction');

            this.props.onEnd(this.props.index);
        }
    }

    ref = player => {
        this.player = player
    }

    render() {

        // Hide player if it is not yet this video parts turn.
        const styleHide = !this.props.media.active ? {display: 'none'} : {};
        var muted = false;
        var controls = false;
        var progressInterval = 50;

        return (
            <Row>
                <Col xs={12}>
                    <ReactPlayer
                        ref={this.ref}
                        style={styleHide}
                        width="100%"
                        height="100%"
                        url={this.props.media.src}
                        controls={controls}
                        muted={muted}
                        playing={this.props.media.playing}
                        progressInterval={progressInterval}
                        onReady={this.onReady}
                        onPlay={this.onPlay}
                        onStart={this.onStart}
                        onDuration={this.onDuration}
                        onProgress={this.onProgress}
                        config={{
                            file: {
                                hlsOptions: {
                                    debug: false,
                                    hlsVersion: "0.12.4",
                                    forceHLS: false,
                                    maxMaxBufferLength: 3,
                                    maxBufferLength: 3,
                                    startPosition: this.props.media.start / 1000,
                                    // sign each request with aws signature 4 (signed headers)
                                    xhrSetup: function (xhr, url) {
                                        var headers = signV4headers(url, S3.region, S3.accessKey, S3.secretKey);
                                        for (var h in headers) {
                                            if (headers.hasOwnProperty(h)) {
                                                xhr.setRequestHeader(h, headers[h])
                                            }
                                        }
                                    },
                                },
                            },
                        }}>
                    </ReactPlayer>
                </Col>
            </Row>
        )
    }
}


class PlayerYouTube extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            duration: 0,
            seeking: false,
            first: true
        };


        this.player = React.createRef();
    }

    componentDidMount(){
        console.log('PlayerYouTube componentDidMount');
        console.log('seekTo', this.props.media.start / 1000);
        this.player.seekTo(this.props.media.start / 1000);

    }

    componentDidUpdate(prevProps) {
        //console.log('PlayerYouTube componentDidUpdate ', this.props.index,  this.state.progressState);
        if (!this.props.media.playing) {
            console.log('seekTo', this.props.media.start / 1000);
            this.player.seekTo(this.props.media.start / 1000);
        }
    }


    youtubeURL(videoID) {
        return "https://www.youtube.com/watch?v=" + videoID
    }

    onReady = () => {
        //console.log('onReady');

    };

    onStart = () => {
        //console.log('onStart');
    };

    onPlay = () => {
        //console.log('onPlay');

    };

    onDuration = (duration) => {
        //console.log('onDuration', duration);

    };

    onPause = () => {
        //console.log('onPause');

    }

    onEnded = () => {
        //console.log('onEnded');
    }

    onProgress = progressState => {

        //console.log('onProgress', this.props.index, progressState);

        this.setState((state) => {

            state.progressState = progressState

            return {...state}
        });


        if (progressState.playedSeconds >= (this.props.media.end / 1000)) {
            console.log('onProgress above end', progressState.playedSeconds, this.props.media.end / 1000);

            this.props.onEnd(this.props.index);

            this.player.seekTo(this.props.media.start / 1000);
        }

    };

    onSeek = seconds => {
        //console.log('onSeek');
    };

    ref = player => {
        this.player = player
    };

    render() {

        // Hide player if it is not yet this video parts turn.
        const styleHide = !this.props.media.active ? {display: 'none'} : {};
        var muted = false;
        var controls = false;
        var progressInterval = 50;

        // play youtube video from specified start- to endtime.
        return (
            <Row>
                <Col xs={12}>
                    <div className='player-wrapper' style={styleHide}>
                        <ReactPlayer className='react-player'
                                     ref={this.ref}
                                     style={styleHide}
                                     width="100%"
                                     height="100%"
                                     url={this.youtubeURL(this.props.media.src)}
                                     playing={this.props.media.playing}
                                     controls={controls}
                                     muted={muted}
                                     config={{
                                         youtube: {
                                             playerVars: {
                                                 modestbranding: 1,
                                                 fs: 0,
                                                 cc_load_policy: 1,
                                                 hl: "en"
                                             },
                                             preload: true
                                         }
                                     }}
                                     progressInterval={progressInterval}
                                     onDuration={this.onDuration}
                                     onProgress={this.onProgress}
                                     onReady={this.onReady}
                                     onPlay={this.onPlay}
                                     onStart={this.onStart}
                                     onPause={this.onPause}
                                     onEnded={this.onEnded}
                                     onSeek={this.onSeek}>
                        </ReactPlayer>
                    </div>
                </Col>
            </Row>
        )
    }
}

class PlayerVideoFile extends React.Component {

    constructor(props) {
        super(props);

        this.player = React.createRef();
    }

    onReady = () => {
        //console.log('onReady');
    }

    onStart = () => {
        //console.log('onStart');
    }

    onPlay = () => {
        this.player.seekTo(this.props.media.start / 1000);
        //console.log('onPlay');
    }

    // reset scene of video files to start (not used by streaming)
    onEnded = state => {
        //console.log("onEnded")
        this.player.seekTo(0);
        this.props.onEnd(this.props.index);
    }

    ref = player => {
        this.player = player
    }

    render() {

        // Hide player if it is not yet this video parts turn.
        const styleHide = !this.props.media.active ? {display: 'none'} : {};
        var muted = false;
        var controls = false;

        // play entire video
        return (
            <Row>
                <Col xs={12}>
                    <ReactPlayer
                        ref={this.ref}
                        style={styleHide}
                        width="100%"
                        height="100%"
                        url={this.props.media.src}
                        controls={controls}
                        muted={muted}
                        playing={this.props.media.playing}
                        onReady={this.onReady}
                        onPlay={this.onPlay}
                        onStart={this.onStart}
                        onEnded={this.onEnded}>
                    </ReactPlayer>
                </Col>
            </Row>
        )
    }
}



function ActionBar(props) {

    return (
        <Row className='ActionBar'>
            <Col xs={4} className='ReducedPadding'>
                <div className="text-center">
                    <ActionButton name="Play" handleClick={props.handlePlayButton}/>
                </div>
            </Col>
            <Col xs={4} className='ReducedPadding'>
                <div className="text-center">
                    <ActionButton name="Next" handleClick={props.onButtonNextPress}/>
                </div>
            </Col>
            <Col xs={4} className='ReducedPadding'>
                <div className="text-center">
                    <ShareButton onButtonSharePress={props.onButtonSharePress} videoRecipe={props.currentVideo}/>
                </div>
            </Col>
        </Row>
    )
}

function ShareVideoModal(props) {
    return (

            <Modal
                {...props}
                aria-labelledby="contained-modal-title-vcenter"
                centered
                style={{ "padding-right": "10px", "padding-left": "10px"}}

            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Share a Video
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row className="show-grid">
                            <div className="d-flex bd-highlight">
                                <div className="p-2 flex-fill bd-highlight text-break">
                                    <code>
                                        {props.sharetext}
                                    </code>
                                </div>
                                <div className="p-2 flex-fill bd-highlight">
                                    <CopyToClipboard text={props.sharetext}>
                                        <Button variant="dark" onClick={props.onButtonCopyPress}>COPY</Button>
                                    </CopyToClipboard>
                                </div>
                            </div>

                        </Row>
                    </Container>
                </Modal.Body>

            </Modal>

    );
}


function ShareButton(props) {
    const [modalShow, setModalShow] = React.useState(false);

    // url to be shared
    let url = window.location.origin.toString() + '?v=' + props.videoRecipe;

    return (
        <ButtonToolbar>
            <ActionButton name="Share" handleClick={() => setModalShow(true)}/>

            <ShareVideoModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                sharetext={url}
                onButtonCopyPress={props.onButtonSharePress}

            />
        </ButtonToolbar>
    )
}


function ActionButton(props) {
    return (
        <Button block='true' variant="dark" onClick={props.handleClick}>{props.name}</Button>
    )
}

function VideoInfoBlock(props) {

    return (
        <Row>
            <Col xs={12} className='ReducedPadding'>
                <TargetTextBlock text={props.userQuery}/>
                <PartList partsData={props.info.parts}/>
            </Col>
        </Row>
    )
}


function TargetTextBlock(props) {
    return (
        <Card bg='warning' id={'composey_card'}>
            <Card.Body id={'composey_card'}>{props.text}</Card.Body>
        </Card>
    )
}

function PartList(props) {
    return (
        <Card>
            <ListGroup variant="flush">
                {props.partsData.map((part, index) =>
                    <PartInfo key={index} data={part}/>
                )}
            </ListGroup>
        </Card>
    )
}

function PartInfo(props) {

    let text = props.data.subText.replace(/<em>/g, '<b>');
    text = text.replace(/<\/em>/g, '</b>');

    return (
        <ListGroup.Item variant="secondary">
            <div className="col-xs-12 text-truncate">
                <div dangerouslySetInnerHTML={{__html: text}}/>
            </div>
            <div className="col-xs-12 text-truncate">{props.data.movie}</div>
        </ListGroup.Item>
    )

}


// Component that owns the state

export function Composition(props) {

    function currentVideo() {
        return props.videoCandidates[props.currentVideo.videoCandidatesRef]
    }

    return (
        <Container style={{ "padding-right": "0", "padding-left": "0"}}>
            <Container>
                <TopBar handlePlaylistChange={props.handlePlaylistChange}
                        playlists={props.playlists}
                        currentPlaylist={props.currentPlaylist}/>
                <QueryForm onQueryFormSubmit={props.handleQueryFormSubmit}/>
            </Container>

            <Container style={{ "padding-right": "0", "padding-left": "0"}}>

                <Video onEnd={props.handleSceneEnd} media={props.currentVideo.media} s3={props.s3}
                       candidateNumber={props.currentVideo.videoCandidatesRef}/>
            </Container>

            <Container>
                <ActionBar onButtonNextPress={props.handleButtonNextPress}
                           onButtonSharePress={props.handleButtonSharePress}
                           currentVideo={currentVideo()}
                           handlePlayButton={props.handlePlayButton}/>
                <VideoInfoBlock userQuery={props.userQuery} info={props.currentVideo.info}/>
            </Container>

        </Container>
    );

}