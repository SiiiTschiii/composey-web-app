import 'crypto-js';

var crypto = require("crypto-js");

var myService = 's3';
var myMethod = 'GET';
var payload = '';

function getSignatureKey(keyDirty, dateStampDirty, regionNameDirty, serviceNameDirty) {

    var key = crypto.enc.Utf8.parse('AWS4' + keyDirty);
    var dateStamp = crypto.enc.Utf8.parse(dateStampDirty);
    var regionName = crypto.enc.Utf8.parse(regionNameDirty);
    var serviceName = crypto.enc.Utf8.parse(serviceNameDirty);
    var kDate = crypto.HmacSHA256(dateStamp, key);
    var kRegion = crypto.HmacSHA256(regionName, kDate);
    var kService = crypto.HmacSHA256(serviceName, kRegion);
    return crypto.HmacSHA256("aws4_request", kService);
}

// this function converts the generic JS ISO8601 date format to the specific format the AWS API wants
function getAmzDate(dateStr) {
    var chars = [":","-"];
    for (var i=0;i<chars.length;i++) {
        while (dateStr.indexOf(chars[i]) !== -1) {
            dateStr = dateStr.replace(chars[i],"");
        }
    }
    dateStr = dateStr.split(".")[0] + "Z";
    return dateStr;
}

// Signs and returns the AWS Signature Version 4 headers
export function signV4headers(url, region, access_key, secret_key) {
    var s3URL = new URL(url)
    // get the various date formats needed to form our request
    var amzDate = getAmzDate(new Date().toISOString());
    var datestamp = amzDate.split("T")[0];

    // we have an empty payload here because it is a GET request
    // get the SHA256 hash value for our payload
    var hashedPayload = crypto.SHA256(payload).toString();

    // create our canonical request
    var canonicalReq =  myMethod + '\n' +
        s3URL.pathname + '\n' +
        '\n' +
        'host:' + s3URL.hostname + '\n' +
        'x-amz-date:' + amzDate + '\n' +
        '\nhost;x-amz-date\n' +
        hashedPayload;

    // hash the canonical request
    var canonicalReqHash = crypto.SHA256(canonicalReq).toString();

    // form our String-to-Sign
    var stringToSign =  'AWS4-HMAC-SHA256\n' +
        amzDate + '\n' +
        datestamp+'/'+region+'/'+myService+'/aws4_request\n'+
        canonicalReqHash;

    // get our Signing Key
    var signingKey = getSignatureKey(secret_key, datestamp, region, myService);

    // Sign our String-to-Sign with our Signing Key
    var authKey = crypto.HmacSHA256(stringToSign, signingKey);

    // Form our authorization header
    var authString  = 'AWS4-HMAC-SHA256 ' +
        'Credential='+
        access_key+'/'+
        datestamp+'/'+
        region+'/'+
        myService+'/aws4_request, '+
        'SignedHeaders=host;x-amz-date, '+
        'Signature='+authKey;

    // throw our headers together
    return {
        'Authorization' : authString,
        'x-amz-date' : amzDate,
        'x-amz-content-sha256' : hashedPayload
    };
    
}
