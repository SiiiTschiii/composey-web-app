import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter} from 'react-router-dom';
import {Switch, Route} from 'react-router-dom';
import {Settings} from './scenes/settings/settings.js';
import {Composition} from './scenes/home/home.js';
import {withAlert} from './components/alert.js';

// initial state and static config data
const queryString = require('query-string');

let Setting = withAlert(Settings);
let Home = withAlert(Composition);


const InitState = {
    messageComposition: {
        playing: false,
        userQuery: "",
        videoCandidates: [],
        currentVideo: {
            videoCandidatesRef: null,
            media: [],
            info: {
                parts: [],
            }
        },
        parameters: {
            weightNumParts: 2.0,
            weightBalanced: 1.0,
            weightSimilarity: 10.0,
            weightSplits: 100.0
        },
        playlists: [],
        currentPlaylist: 0,
    },
    error: ''
};

class App extends React.Component {

    constructor(props) {
        super(props);

        // define init state
        this.state = InitState;

        this.handleQueryFormSubmit = this.handleQueryFormSubmit.bind(this);
        this.handleButtonNextPress = this.handleButtonNextPress.bind(this);
        this.handleSceneEnd = this.handleSceneEnd.bind(this);
        this.handleMsgCompParameterChange = this.handleMsgCompParameterChange.bind(this);
        this.handleErrors = this.handleErrors.bind(this);
        this.handleButtonSharePress = this.handleButtonSharePress.bind(this);
        this.handlePlayButton = this.handlePlayButton.bind(this);
        this.handlePlaylistChange = this.handlePlaylistChange.bind(this);
    }

    async componentDidMount() {
        console.log("componentDidMount")

        await this.getPlaylists()

        if ('v' in queryString.parse(this.props.location.search)) {
            // load video if given as url parameter

            this.setState((state) => {
                let newState = JSON.parse(JSON.stringify(state));
                newState['messageComposition']['videoCandidates'] = [queryString.parse(this.props.location.search).v];

                return {...newState}
            }, function() {

                this.createNextVideo(false)
            });




        } else {

            this.createDefaultVideo()
        }
    }

    componentDidUpdate(prevProps) {
        console.log('App componentDidUpdate');
    }

    createDefaultVideo() {
        console.log('createDefaultVideo')
        this.handleQueryFormSubmit("hello obama", false)

    }

    async getPlaylists() {
        console.log('getPlaylists')
        try {
            let response = await fetch('/playlists?', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                }
            })

            await this.handleErrors(response)

            let responseJSON = await response.json()

            this.setState((state) => {

                let newState = JSON.parse(JSON.stringify(state));
                newState['messageComposition']['playlists'] = responseJSON.data.playlists;

                return {...newState}
            });

        } catch(error) {
            console.log(error);
        }
    }

    // handleButtonNextPress is called when the next button is pressed
    handleButtonNextPress() {
        console.log('handleButtonNextPress');
        this.createNextVideo(true)
    }

    handleErrors(response) {
        console.log('handleErrors')
        if (!response.ok) {
            response.json().then(data => {
                if (data.error) {
                    this.setState((state) => {
                        let newState = JSON.parse(JSON.stringify(state));
                        newState['error'] = data.error;

                        return {...newState}
                    });
                }
            });

            throw Error(response.statusText);
        }
        return response;
    }

    // createNextVideo creates the next video
    createNextVideo(directPlay) {
        console.log("createNextVideo")

        if (this.state.messageComposition.videoCandidates.length === 0) {
            return
        }

        let next_num;
        // when new composition -> new candidates list
        if (this.state.messageComposition.currentVideo.videoCandidatesRef == null) {
            next_num = 0
            // end of candidates list reached
        } else if (this.state.messageComposition.currentVideo.videoCandidatesRef === this.state.messageComposition.videoCandidates.length - 1) {

            // is it a single shared video..
            if (this.state.messageComposition.currentVideo.videoCandidatesRef === 0) {

                // then compose more of the same video candidates
                this.handleQueryFormSubmit(this.state.messageComposition.userQuery, false);
                return

            } else {
                // otherwise restart with the first in the list
                next_num = 0
            }
            // next candidate in list
        } else {
            next_num = this.state.messageComposition.currentVideo.videoCandidatesRef + 1
        }

        console.log("fetch")
        fetch(`/videos?v=${this.state.messageComposition.videoCandidates[next_num]}`, {
            method: 'GET',

            headers: {
                'Accept': 'application/json',
            }
        })
            .then(this.handleErrors)
            .then(response => response.json())
            .then(json => {

                this.setState((state) => {

                    let newState = JSON.parse(JSON.stringify(state));
                    newState['messageComposition']['userQuery'] = json.data.video.query;
                    newState['messageComposition']['currentVideo']['videoCandidatesRef'] = next_num;
                    newState['messageComposition']['currentVideo']['media'] = json.data.video.media.map((media, index) => {
                        let active = false;
                        let playing = false;
                        if (index === 0) {
                            active = true;
                            playing = directPlay;
                        }

                        return {
                            src: media.src,
                            start: media.start,
                            end: media.end,
                            type: media.type,
                            active: active,
                            playing: playing
                        }
                    });
                    newState['messageComposition']['currentVideo']['info'] = json.data.video.info;
                    newState['messageComposition']['playing'] = directPlay;
                    newState['error'] = '';

                    return {...newState}

                });
            })
            .catch(error => {
                console.log(error);
            })
    }

    clientSideValidation(queryText) {
        if (queryText.split(' ').length > 10) {

            return 'Message length exceeds max length'
        }
        return ''
    }

    createNextVideoFunction(playDirect) {
        return function() {
            this.createNextVideo(playDirect);
        }
    }

    handleQueryFormSubmit(queryText, userTriggered) {
        console.log('handleQueryFormSubmit')

        let error = this.clientSideValidation(queryText);

        this.setState((state) => {
            let newState = JSON.parse(JSON.stringify(state));
            newState['error'] = error;
            // unset media before request to avoid reloading of old videos
            newState['messageComposition']['currentVideo']['media'] = [];
            return {...newState}
        });

        // compose message
        if (!error) {

            let body = {
                query: queryText,
                playlistID: this.state.messageComposition.playlists[this.state.messageComposition.currentPlaylist].id,
                parameters: this.state.messageComposition.parameters
            };

            fetch('/compositions', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            })
                .then(this.handleErrors)
                .then(response => response.json())
                .then(json => {
                    this.setState((state) => {
                        let stateCopy = JSON.parse(JSON.stringify(state));
                        stateCopy['messageComposition']['videoCandidates'] = json.data.candidates;
                        stateCopy['messageComposition']['currentVideo']['videoCandidatesRef'] = null;
                        return {
                            ...stateCopy
                        }
                    }, this.createNextVideoFunction(true));   // callback function that creates the video as soon as the state is updated.
                })
                .catch(error => console.log(error));

            if (userTriggered) {
                this.submitEvent(this.createEventBody("compositions", body))
            }
        }
    }

    submitEvent(body) {
        console.log('submitEvent');

        // compose message
        fetch('https://av90wuebxl.execute-api.eu-central-1.amazonaws.com/prod/clicks', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }).catch(error => console.log(error))

    }


    handleSceneEnd(sceneIndex) {
        console.log("handleSceneEnd: scene " + sceneIndex);

        // Make sure scene end is only once handled
        if (this.state.messageComposition.currentVideo.media[sceneIndex].active === true) {

            // update state
            this.setState((state) => {

                const newState = JSON.parse(JSON.stringify(state));

                // determine ended scene and deactivate it
                var nextActiveScene = sceneIndex + 1;
                var nextMedia = state.messageComposition.currentVideo.media;
                nextMedia[sceneIndex].active = false;
                nextMedia[sceneIndex].playing = false;

                // activate next scene if exists
                if (nextActiveScene <= state.messageComposition.currentVideo.media.length - 1) {
                    nextMedia[nextActiveScene].active = true;
                    nextMedia[nextActiveScene].playing = true
                } else {
                    // show player of first scene at end but without replaying
                    nextMedia[0].active = true;
                    nextMedia[0].playing = false;
                    newState['messageComposition']['playing'] = false
                }

                newState['messageComposition']['currentVideo']['media'] = nextMedia;
                return {...newState}
            })
        }

    }

    handlePlayButton() {
        console.log("handlePlayButton")

        // Don't replay when play ongoing
        if (this.state.messageComposition.playing === false) {
            this.setState((state) => {

                // Play first scene
                const newState = JSON.parse(JSON.stringify(state));
                newState['messageComposition']['currentVideo']['media'] = newState.messageComposition.currentVideo.media.map((media, index) => {
                    let active = false;
                    let playing = false;
                    if (index === 0) {
                        active = true;
                        playing = true;
                    }

                    return {
                        src: media.src,
                        start: media.start,
                        end: media.end,
                        type: media.type,
                        active: active,
                        playing: playing
                    }
                });
                newState['messageComposition']['playing'] = true
                return {...newState}
            })
        }


    }

    handleMsgCompParameterChange(name, value) {
        console.log("handleMsgCompParameterChange")

        this.setState((state) => {
            const newState = JSON.parse(JSON.stringify(name));
            newState['messageComposition']['parameters'][name] = value;
            return {...newState}
        });
    }

    handlePlaylistChange(idx) {
        console.log("handlePlaylistChange")

        this.setState((state) => {
            const newState = JSON.parse(JSON.stringify(state));
            newState['messageComposition']['currentPlaylist'] = idx;
            return {...newState}
        }, this.createDefaultVideo);


    }

    createEventBody(name, data) {
        console.log("createEventBody")
        return {
            event: name,
            data: data
        }
    }

    handleButtonSharePress() {
        console.log("handleButtonSharePress")

        let currentVideoRecipe = this.state.messageComposition.videoCandidates[this.state.messageComposition.currentVideo.videoCandidatesRef];

        let payload = this.createEventBody(
            "videoShare",
            {
                videoRecipe: currentVideoRecipe
            }
        );

        // compose message
        this.submitEvent(payload)

    }

    render() {

        return (
            <Switch>
                <Route path='/settings' render={(props) =>
                    <Setting
                        parameters={this.state.messageComposition.parameters}
                        error={this.state.error}
                        handleMsgCompParameterChange={this.handleMsgCompParameterChange}/>}
                />
                <Route path='/' render={(props) =>
                    <Home
                        {...this.state.messageComposition}
                        error={this.state.error}
                        handlePlayButton={this.handlePlayButton}
                        handleSettingsChange={this.handleMsgCompParameterChange}
                        handleSceneEnd={this.handleSceneEnd}
                        handleQueryFormSubmit={this.handleQueryFormSubmit}
                        handleButtonNextPress={this.handleButtonNextPress}
                        handleButtonSharePress={this.handleButtonSharePress}

                        handlePlaylistChange={this.handlePlaylistChange}
                        playlists={this.state.messageComposition.playlists}
                        currentPlaylist={this.state.messageComposition.currentPlaylist}
                    />}>
                </Route>
            </Switch>

        )
    }
}

ReactDOM.render((
    <BrowserRouter>
        <div style={{"padding-right": "0", "padding-left": "0", "width": "100%"}}>
            <Route path="/" component={App}/>
        </div>
    </BrowserRouter>
), document.getElementById('root'));