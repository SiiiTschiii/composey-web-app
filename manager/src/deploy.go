package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"gitlab.ethz.ch/chgerber/youtubedl"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func addFieldToAll(c *mongo.Collection) {

	// add the field
	update := bson.M{
		"$set": bson.M{
			"playlist": bson.M{
				"id": "test", "name": "test",
			},
		},
	}

	result, err := c.UpdateMany(context.Background(), bson.M{}, update)
	if err != nil {
		log.Fatal(err)
	}

	num, err := c.CountDocuments(context.Background(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	if result.ModifiedCount != num {
		log.Fatal(result.ModifiedCount)
	}

}

func main() {

	log.SetLevel(log.DebugLevel)

	// connect to db
	c, err := annotation.ConnectMongoDBCollection("mongo", "dev", "testmongodocker")
	if err != nil {
		log.Fatal(err)
	}

	//err = youtubedl.LoadYouTubeAnnotationsPlaylist("PLiZxWe0ejyv9sJVDQVB2savQXKYLaXsWv", "en", c, annotation.Playlist{ID: "PLiZxWe0ejyv9sJVDQVB2savQXKYLaXsWv", Name: "Steven Colbert"})
	//if err != nil {
	//	log.Fatal(err)
	//}

	err = youtubedl.LoadYouTubeAnnotationsPlaylist("PLIcq6QAlz4MQfO7Q6NlSd0oKqdSOlj2zI", "en", c, annotation.Playlist{ID: "PLIcq6QAlz4MQfO7Q6NlSd0oKqdSOlj2zI", Name: "Donald Trump"})
	if err != nil {
		log.Fatal(err)
	}
	//
	//err = youtubedl.LoadYouTubeAnnotationsPlaylist("PLIcq6QAlz4MSiXPMMV9q3Itd0lChE3f-T", "en", c, annotation.Playlist{ID: "PLIcq6QAlz4MSiXPMMV9q3Itd0lChE3f-T", Name: "Arnold Schwarzenegger"})
	//if err != nil {
	//	log.Fatal(err)
	//}

	//err = annotation.LoadS3AnnotationsStream("/tmp", "eu-central-1", "composey-media", "hls/TheBigBangTheory", c,  annotation.Playlist{ID: "tbbt", Name: "The Big Bang Theory"})
	//if err != nil {
	//	log.Fatal(err)
	//}

	err = c.Database().Client().Disconnect(context.Background())

	if err != nil {
		log.Fatal(err)
	}
}
