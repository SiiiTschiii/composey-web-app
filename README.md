## Composey Web Application

[![pipeline status](https://gitlab.ethz.ch/chgerber/arniWebApp/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/arniWebApp/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/arniWebApp/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/arniWebApp/commits/master)

Web Application that Composes Messages from Movies

### Project structure

    .
    ├── client                 # ReactJS web application (Frontend)
    ├── server                 # Go web server (Backend)
    ├── testData               # Files for unit tests
    ├── docker-compose.yml     # Docker setup for local deployment of composey
    ├── rsa-key                # ssh key for access to `gitlab.ethz.ch private` repos by `docker-compose.yml`
    ├── .gitlab-ci.yml         # Gitlab continuous integration script   
    └── README.md

### Run locally

### Run App
Requires Docker and docker-compose to be installed.
Expects a file called `rsa-key` in the project root directory. The content of the file has to be a ssh private key without password, with access rights to the `gitlab.ethz.ch` dependency go repos.

```bash
docker-compose up --build
# Will take some time for the first time
# Then open browser at localhost:3001
```

### Load Videos initially

```
docker-compose -f manager/manager.yml build
docker-compose -f manager/manager.yml run --rm manager
```