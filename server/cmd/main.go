package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app"
)

func main() {

	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)

	// Run app
	a := app.App{}
	boolPtr := flag.Bool("prod", false, "Provide this flag in production. This ensures that a config file is provided before the application starts.")
	configFilePtr := flag.String("config", "", "Provide the path to the config file. Is required when `prod` mode. Otherwise default config is assumed.")
	a.Initialize(boolPtr, configFilePtr)
	a.Run()
}
