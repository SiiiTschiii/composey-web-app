FROM golang:1.13.5-alpine

RUN apk add --update git make bash openssh-client curl
COPY rsa-key /root/.ssh/id_rsa
RUN eval $(ssh-agent -s) && cat /root/.ssh/id_rsa | tr -d '\r' | ssh-add - > /dev/null
RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh
RUN git config --global url."ssh://git@gitlab.ethz.ch/".insteadOf "https://gitlab.ethz.ch/"
RUN git config --global user.email "gitlab-ci@composey.io"
RUN git config --global user.name "gitlab-ci composey"
RUN [[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
RUN apk add --update \
    python \
    python-dev \
    py-pip \
    build-base \
    && apk --purge -v del py-pip \
    && rm -rf /var/cache/apk/*
RUN wget "https://yt-dl.org/downloads/latest/youtube-dl" -O "/usr/local/bin/youtube-dl"
RUN chmod a+rx "/usr/local/bin/youtube-dl"

WORKDIR /code

# Cache optimization
COPY go.mod .
COPY go.sum .

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download

COPY server server
COPY config/wait_to_start.sh config/wait_to_start.sh

# Build server
RUN cd server/cmd; CGO_ENABLED=0 go build -o server