package usereventprocessing

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/videorecipe"
	"gitlab.ethz.ch/chgerber/monitor"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
)

func updateAnnotation(docID string, text string, c *mongo.Collection) error {

	// increment field "shared" when existent
	done, err := findAndIncrement(docID, text, c)
	if err != nil {
		return err
	}

	// create field shared and set to 1
	if !done {
		err = createAndIncrement(docID, text, c)
		if err != nil {
			return err
		}
	}

	return nil
}

func findAndIncrement(docID string, text string, c *mongo.Collection) (done bool, err error) {
	id, err := primitive.ObjectIDFromHex(docID)
	if err != nil {
		return false, err
	}

	// increment the field if it exists
	findFilter := bson.M{
		"_id": bson.M{
			"$eq": id,
		},
		"usage.text": bson.M{
			"$eq": text,
		},
	}

	// increment sharedCount
	update := bson.M{
		"$inc": bson.M{
			"usage.$.shared": 1,
		},
	}

	result, err := c.UpdateOne(context.Background(), findFilter, update)
	if err != nil {
		return false, err
	}

	if result.ModifiedCount == int64(0) {
		return false, nil
	}

	return true, nil
}

func createAndIncrement(docID string, text string, c *mongo.Collection) error {
	id, err := primitive.ObjectIDFromHex(docID)
	if err != nil {
		return err
	}

	findFilter := bson.M{
		"_id": bson.M{
			"$eq": id,
		},
	}

	// add the field
	update := bson.M{
		"$push": bson.M{
			"usage": bson.M{
				"text": text, "shared": 1,
			},
		},
	}

	result, err := c.UpdateOne(context.Background(), findFilter, update)
	if err != nil {
		return err
	}

	if result.ModifiedCount == int64(0) {
		return fmt.Errorf("didn't modify document %s", docID)
	}

	return nil
}

func handleShareVideo(eventData json.RawMessage) error {
	var evnt eventVideoShare

	err := json.Unmarshal(eventData, &evnt)
	if err != nil {
		return err
	}

	video, err := videorecipe.Decode(evnt.VideoRecipe)
	if err != nil {
		return err
	}

	c, err := annotation.ConnectMongoDBCollection(os.Getenv("MONGO_HOST"), os.Getenv("MONGO_DB_NAME"), os.Getenv("MONGO_COL_NAME"))
	if err != nil {
		return err
	}

	// for each scene update the shared counter of the corresponding annotation
	for _, scene := range video.Scenes {

		_, _, fn := monitor.CallerRef(0)
		log.WithFields(log.Fields{
			"fn": fn,
			"id": scene.DocID,
		}).Debug("update annotation")

		err := updateAnnotation(scene.DocID, scene.Text, c)
		if err != nil {
			return err
		}

	}

	return nil
}

type eventVideoShare struct {
	VideoRecipe string `json:"videoRecipe"`
}

// ProcessEvent detect the event type and processes the event data accordingly
// expects json with the following structure:
/*
{
	"event": "$eventName",
	"data": {...}

}
*/
func ProcessEvent(payload []byte) error {

	var b body

	err := json.Unmarshal(payload, &b)
	if err != nil {
		return err
	}

	switch b.Event {

	case "videoShare":
		err := handleShareVideo(b.Data)
		if err != nil {
			return err
		}

	default:
		log.WithFields(log.Fields{
			"event": b.Event,
		}).Debug("unhandled event")
	}

	return nil
}

type body struct {
	Event string          `json:"event"`
	Data  json.RawMessage `json:"data"`
}
