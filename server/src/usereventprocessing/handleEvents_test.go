package usereventprocessing

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	os.Setenv("MONGO_HOST", "composey-y6t4l.mongodb.net")
	os.Setenv("MONGO_DB_NAME", "dev")
	os.Setenv("MONGO_COL_NAME", "test")

	os.Exit(m.Run())
}

func TestStreamProcessor_ProcessEventValid(t *testing.T) {

	event := `
	{
		"event": "videoShare",
		"data": {
			"videoRecipe": "BWhlbGxvAV0xfMrtX0gxUnXDSwV0aGVyZQFdMXzK7V9IMVJ1wlE="
		}
	}`

	err := ProcessEvent([]byte(event))

	assert.Equal(t, nil, err)

}

func TestStreamProcessor_ProcessEventInvalid_EventMissing(t *testing.T) {

	event := `
	{
		"data": {
			"videoRecipe": "BWhlbGxvAV0xfMrtX0gxUnXDSwV0aGVyZQFdMXzK7V9IMVJ1wlE="
		}
	}`

	err := ProcessEvent([]byte(event))

	assert.Equal(t, nil, err)

}

func TestStreamProcessor_ProcessEventInvalid_NoData(t *testing.T) {

	event := `
	{	
		"event": "videoShare"
	}`

	err := ProcessEvent([]byte(event))

	assert.Error(t, err)

}

func TestStreamProcessor_ProcessEventInvalid_NoVideoRecipe(t *testing.T) {

	event := `
	{	
		"event": "videoShare",
		"data": {
		}
	}`

	err := ProcessEvent([]byte(event))

	assert.Equal(t, nil, err)

}

func TestStreamProcessor_ProcessEventInvalid_InvalidJson(t *testing.T) {

	event := `
	{
		"data": {
			"videoRecipe": "BWhlbGxvAV0xfMrtX0gxUnXDSwV0aGVyZQFdMXzK7V9IMVJ1wlE="
		
	}`

	err := ProcessEvent([]byte(event))

	assert.Error(t, err)

}

func TestStreamProcessor_UpdateMongoDBArrayElementExisting(t *testing.T) {

	c, err := annotation.ConnectMongoDBCollection(os.Getenv("MONGO_HOST"), os.Getenv("MONGO_DB_NAME"), os.Getenv("MONGO_COL_NAME"))

	assert.Equal(t, nil, err)

	done, err := findAndIncrement("5d317ccaed5f48315275c34b", "hello", c)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, done)
}

func TestStreamProcessor_UpdateMongoDBArrayNotExisting(t *testing.T) {

	a := &annotation.Annotation{
		ID: primitive.NewObjectID(),
		Subtitle: annotation.Subtitle{
			Text: "should i go",
		},
	}

	c, err := annotation.ConnectMongoDBCollection(os.Getenv("MONGO_HOST"), os.Getenv("MONGO_DB_NAME"), os.Getenv("MONGO_COL_NAME"))
	assert.Equal(t, nil, err)

	annotation.UploadToDB([]*annotation.Annotation{a}, c)

	docID := a.ID.Hex()
	text1 := "should i"
	text2 := "should i go"

	done, err := findAndIncrement(docID, text1, c)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, done)
	err = createAndIncrement(docID, text1, c)
	assert.Equal(t, nil, err)

	done, err = findAndIncrement(docID, text2, c)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, done)
	err = createAndIncrement(docID, text2, c)
	assert.Equal(t, nil, err)

	//delete the field
	id, err := primitive.ObjectIDFromHex(docID)
	assert.Equal(t, nil, err)

	findFilter := bson.M{
		"_id": bson.M{
			"$eq": id,
		},
	}

	c.DeleteOne(context.Background(), findFilter)

}
