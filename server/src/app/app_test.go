package app

import (
	"bytes"
	"flag"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/controllers"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"io/ioutil"
	"net/http/httptest"
	"os"
	"testing"
)

var a App

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	// Setup app in dev mode
	a = App{}
	boolPtr := flag.Bool("prod", false, "Provide this flag in production. This ensures that a .config file is provided before the application starts.")
	configFilePtr := flag.String("config", "", "Provide the path to the config file. Is required when `prod` mode. Otherwise default config is assumed.")
	a.Initialize(boolPtr, configFilePtr)

	// Run the app as go routine
	go a.Run()

	code := m.Run()

	os.Exit(code)
}

func TestEmptyQuery(t *testing.T) {
	compositionsC := controllers.NewCompositions(models.NewCompositionService(config.ElasticConfig{}))

	handler := compositionsC.Create
	req := httptest.NewRequest("POST", "http://localhost/compositions", bytes.NewBufferString(`{"query":""}`))

	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	assert.Equal(t, 400, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, `{"error":"request is malformed"}`, string(body), "Body not as expected")
	assert.Equal(t, "400 Bad Request", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")
}
