package views

// CompositionsResponse describes the HTTP response body field data of the GET /compositions API endpoint
type CompositionsResponse struct {
	Candidates []string `json:"candidates,required"`
}
