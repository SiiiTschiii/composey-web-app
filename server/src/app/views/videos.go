package views

import "gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"

// VideosResponse describes the HTTP response body field data of the POST /video API endpoint
type VideosResponse struct {
	Video Video `json:"video,required"`
}

// Video describes the video candidate of the response
type Video struct {
	Query string         `json:"query,required"`
	ID    string         `json:"id,required"`
	Media []models.Media `json:"media,required"`
	Info  Info           `json:"info,required"`
}

// Info contains meta information of all scenes of the video candidate in the POST /video API endpoint response
type Info struct {
	Parts []Part `json:"parts,required"`
}

// Part contains meta information of one specific scene of the video candidate of the POST /video API endpoint response
type Part struct {
	Text    string `json:"text,required"`
	SubText string `json:"subText,required"`
	Movie   string `json:"movie,required"`
	Start   int64  `json:"start,required"`
	End     int64  `json:"end,required"`
}
