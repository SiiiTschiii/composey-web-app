package views

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/errors"
	"net/http"
)

// NewResponse creates an empty Response with StatusCode=200
func NewResponse() *Response {
	return &Response{
		StatusCode: 200,
	}
}

// Response is the HTTP response type
type Response struct {
	Data       interface{} `json:"data,omitempty"`
	Error      interface{} `json:"error,omitempty"`
	StatusCode int         `json:"-"`
}

func (r *Response) setError(err error) {
	var msg string
	log.Infof("%T: %s", err, err.Error())
	if pErr, ok := err.(errors.HTTPError); ok {
		msg = pErr.Public()
		r.StatusCode = pErr.Code()
	} else {
		msg = errors.ServerErrMsg
		r.StatusCode = 500
	}
	r.Error = msg
}

// Render writes the data to the HTTP response
func (r *Response) Render(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.StatusCode)

	r.Data = data

	body, err := json.Marshal(r)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(body)
	if err != nil {
		w.WriteHeader(500)
		return
	}
}

// RenderError writes the error to the HTTP response
func (r *Response) RenderError(w http.ResponseWriter, err error) {
	r.setError(err)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.StatusCode)
	body, err := json.Marshal(r)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(body)
	if err != nil {
		w.WriteHeader(500)
		return
	}
}
