package views

// PlaylistResponse describes the HTTP response body field data of the POST /playlist API endpoint
type PlaylistResponse struct {
	Playlists []Playlist `json:"playlists,required"`
}

// Playlist describes the playlist in the response
type Playlist struct {
	Name string `json:"name,required"`
	ID   string `json:"id,required"`
}
