# REST API v1

URI's relative to: 
```
 https://www.arnies.tv/v1
```
## Compositions
### Request
```
POST /compositions
```
#### Parameters

The weights parameters are of type float. They are optional. If not passed default values are taken.
```json

 {
    "query": "hello there",
    "parameters": {
        "weightNumParts": 2.0,
        "weightBalanced": 1.0,
        "weightSimilarity": 10.0,
        "weightSplits": 100.0 
    },
    "playlistID": "tbbt"
}

```
#### Response
```json
{
    "data": {
        "canidates": [
            "C2hlbGxvIHRoZXJlAVxTLHkse05CaCE-jQ==",
            "BWhlbGxvAVxTLHYse05CaCEZ7QV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHcse05CaCEfWgV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHcse05CaCEloAV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHcse05CaCElrwV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHcse05CaCEpagV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHkse05CaCE88AV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHkse05CaCE88QV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHkse05CaCE-EAV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHkse05CaCFA9wV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHose05CaCFPsQV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHose05CaCFQ0AV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHose05CaCFTJAV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHsse05CaCFYnAV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHsse05CaCFZjwV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHsse05CaCFarwV0aGVyZQFcUyx6LHtOQmghSus=",
            "BWhlbGxvAVxTLHsse05CaCFiBwV0aGVyZQFcUyx6LHtOQmghSus="
        ]
    }
}
```

## Videos
### Request
```
GET /videos
```
#### Parameters
```json
{
  "v": "C2hlbGxvIHRoZXJlAVxTLHkse05CaCE-jQtsZXQncyBwYXJ0eQFcUyyMLHtOQmgidOoDYnllAVxTLHcse05CaCEfxg=="
}
```
#### Response
```json
{
	"data": {
		"query": "Hello there. Let's party. Bye",
		"id": "C2hlbGxvIHRoZXJlAVxTLHkse05CaCE-jQtsZXQncyBwYXJ0eQFcUyyMLHtOQmgidOoDYnllAVxTLHcse05CaCEfxg==",
		"media": [
		    {
				"src": "https://s3.eu-central-1.amazonaws.com/arnie-project-candidates/C2hlbGxvIHRoZXJlAVxTLHkse05CaCE-jQtsZXQncyBwYXJ0eQFcUyyMLHtOQmgidOoDYnllAVxTLHcse05CaCEfxg==.mp4",
				"type": "video/mp4",
				"start": 0,
				"end": 0
			},
			{
				"src": "HXGwJevjOfs",
				"type": "youtube",
				"start": 15000,
				"end": 20000
			},
			{
				"src": "https://s3.eu-central-1.amazonaws.com/arnie-project-media/streaming/hls/master.m3u8",
				"type": "application/x-mpegURL",
				"start": 15000,
				"end": 20000
			}
		],
		"info": {
			"parts": [{
					"text": "Hello there",
					"subText": "<em>Hello there</em>",
					"movie": "The Big Bang Theory - S02E03",
					"start_t": 751426,
					"end_t": 1100
				},
				{
					"text": "Let's party",
					"subText": "<em>Let's party</em> hard",
					"movie": "The Big Bang Theory - S04E09",
					"start_t": 511492,
					"end_t": 1599
				},
				{
					"text": "Bye",
					"subText": "<em>bye</em> bye",
					"movie": "The Big Bang Theory - S05E08",
					"start_t": 137007,
					"end_t": 1000
				}
			]
		}
	}
}
```

## Playlists
### Request
```
GET /playlists
```

#### Response
```json
{
    "data": {
        "playlists": [
            {
                "name": "Donald Trump",
                "id": "PLIcq6QAlz4MQfO7Q6NlSd0oKqdSOlj2zI"
            },
            {
                "name": "Arnold Schwarzenegger",
                "id": "PLIcq6QAlz4MSiXPMMV9q3Itd0lChE3f-T"
            },
            {
                "name": "The Big Bang Theory",
                "id": "tbbt"
            }
        ]
    }
}
```

#### Errors
The API shall avoid sending specific server side error information for the sake of security.
##### Request Error
Code 400
```json
{
    "error": "request is malformed"
}
```

##### Server Error
Code 500
```json
{
    "error": "something went wrong."
}
```