package config

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"os"
)

// Config contains all top level configuration parameter of the app
type Config struct {
	Port            int           `json:"port"`
	Env             string        `json:"env"`
	VideoDir        string        `json:"videoDir"`
	AppFiles        string        `json:"appFiles"`
	WorkDir         string        `json:"workDir"`
	ServerMode      bool          `json:"serverMode"`
	AnnotationStore ElasticConfig `json:"annotationStore"`
	S3              S3Config      `json: "s3"`
}

// ElasticConfig contains Elasticsearch config parameters
type ElasticConfig struct {
	URL   string `json:"url"`
	Index string `json:"index"`
}

// S3Config contains aws S3 storage config parameters
type S3Config struct {
	Region string `json:"region"`
	Bucket string `json:"bucket"`
}

// DefaultElasticConfig returns the default Elasticsearch config
func DefaultElasticConfig() ElasticConfig {
	return ElasticConfig{
		URL:   os.Getenv("ES_URL_TEST"),
		Index: "dev.playlists",
	}
}

// DefaultS3Config returns the default aws S3 storage config parameters
func DefaultS3Config() S3Config {
	return S3Config{
		Region: "eu-central-1",
		Bucket: "composey-media",
	}
}

// IsProd return true if the app is production mode
func (c Config) IsProd() bool {
	return c.Env == "prod"
}

// DefaultConfig returns the default app config
func DefaultConfig() Config {
	return Config{
		Port:            8000,
		Env:             "dev",
		VideoDir:        "/tmp/composey/videos",
		AppFiles:        "client/build",
		WorkDir:         "/tmp/composey/work",
		ServerMode:      false,
		AnnotationStore: DefaultElasticConfig(),
		S3:              DefaultS3Config(),
	}
}

// LoadConfig returns the DefaultConfig if configFile doesn't point on a file
// Returns the parsed config file as type Config otherwise
// Panics when configReq is true AND configFile doesn't point on a file.
// Panics upon error
func LoadConfig(configReq bool, configFile string) Config {
	f, err := os.Open(configFile)
	if err != nil {
		if configReq {
			panic(err)
		}
		log.Println("Using the default config...")
		return DefaultConfig()
	}
	var c Config
	dec := json.NewDecoder(f)
	err = dec.Decode(&c)
	if err != nil {
		panic(err)
	}
	log.Println("Successfully loaded config")
	return c
}
