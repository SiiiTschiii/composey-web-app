package config

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	os.Exit(m.Run())
}

func TestLoadConfig(t *testing.T) {
	c := LoadConfig(true, "test_config.json")
	assert.Equal(t, "prod.fdskjaf2", c.AnnotationStore.Index)
	assert.Equal(t, "https://search-composey-adfdsafdsafs.eu-central-1.es.amazonaws.com", c.AnnotationStore.URL)
}
