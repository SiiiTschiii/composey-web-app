package videorecipe

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/errors"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"strings"
)

// VideoCandidate is the interface that serves as input to videorecipe encoding
type VideoCandidate interface {
	Target() string
	Base3() string
	DocIDs() []string
}

// toBase3Parts splits the base3 representaiton in it's parts.
func toBase3Parts(base3 string) (parts []string) {
	currentPart := string(base3[0])
	if len(base3) == 1 {
		return []string{currentPart}
	}
	for _, c := range base3[1:] {
		if c == '0' {
			currentPart += "0"
		} else {
			parts = append(parts, currentPart)
			currentPart = string(c)
		}
	}
	parts = append(parts, currentPart)

	return parts
}

// docIDToBytest converts the mongoDB id to a bytes array. Returns a encodeURLerror if the docID does not satisfy the mongoDB objcetID format
func docIDToBytes(docID string) ([]byte, error) {
	if len(docID) != 24 {
		return nil, encodeURLError("docID too short")
	}
	b, err := hex.DecodeString(docID)
	if err != nil {
		return nil, encodeURLError(err.Error())
	}
	return b, nil
}

func bytesToDocID(b []byte) string {
	return hex.EncodeToString(b)

}

func encodePart(text string, docID string, present bool) ([]byte, error) {
	var buf bytes.Buffer
	if len(text) > 255 {
		return nil, encodeURLError("part text is too long")
	}
	buf.Write([]byte{uint8(len(text))})
	buf.Write([]byte(text))

	// Add present/missing part indicator
	if !present {
		buf.Write([]byte{uint8(0)})
		return buf.Bytes(), nil
	}
	buf.Write([]byte{uint8(1)})

	docIDBytes, err := docIDToBytes(docID)
	if err != nil {
		return nil, err
	}
	buf.Write(docIDBytes)
	return buf.Bytes(), nil
}

func isMissingPart(part string) bool {
	if part[0] == '2' {
		return true
	}
	return false
}

// Encode creates the videorecipe of the given VideoCandidate
func Encode(vc VideoCandidate) (base64String string, err error) {

	var buffer bytes.Buffer

	base3Parts := toBase3Parts(vc.Base3())

	currentScene := 0
	currentTextPos := 0
	for _, base3Part := range base3Parts {
		currentPartText := util.WordsToString(strings.Fields(vc.Target())[currentTextPos : currentTextPos+len(base3Part)])
		var b []byte
		var err error
		if isMissingPart(base3Part) {
			b, err = encodePart(currentPartText, "", false)

		} else {
			b, err = encodePart(currentPartText, vc.DocIDs()[currentScene], true)
			currentScene++
		}
		if err != nil {
			return "", err
		}
		buffer.Write(b)
		currentTextPos += len(base3Part)
	}

	return base64.URLEncoding.EncodeToString(buffer.Bytes()), nil
}

func decodePart(b *bytes.Buffer) (text string, base3 string, scene models.Scene, err error) {

	// decode text length
	textLength := int(uint8(b.Next(1)[0]))

	// decode text
	if b.Len() < textLength {
		return "", "", models.Scene{}, decodeURLError("buffer smaller than text length indicates")
	}
	text = string(b.Next(textLength))

	// decode present/missing part indicator
	if b.Len() < 1 {
		return "", "", models.Scene{}, decodeURLError("buffer ends before missing/present part indicator")
	}

	switch uint8(b.Next(1)[0]) {
	case 0:
		base3 = "2" + strings.Repeat("0", len(strings.Fields(text))-1)
		return text, base3, models.Scene{}, nil
	case 1:
		base3 = "1" + strings.Repeat("0", len(strings.Fields(text))-1)
	default:
		return "", "", models.Scene{}, decodeURLError("missing/present part indicator is missing")
	}

	// decode doc_id
	if b.Len() < 12 {
		return "", "", models.Scene{}, decodeURLError("buffer ends before doc id")
	}
	docID := bytesToDocID(b.Next(12))

	scene = models.Scene{Text: text, DocID: docID}

	return text, base3, scene, nil
}

// Decode decodes the given videorecipe into the Video
// TODO handle empty string
func Decode(base64String string) (vc *models.Video, err error) {
	b, err := base64.URLEncoding.DecodeString(base64String)
	if err != nil {
		return nil, decodeURLError(err.Error())
	}
	buffer := bytes.NewBuffer(b)

	var scenes []*models.Scene
	var targetText []string
	var base3 string

	for buffer.Len() > 0 {

		textPart, base3Part, scene, err := decodePart(buffer)
		if err != nil {
			return nil, err
		}
		targetText = append(targetText, textPart)
		base3 += base3Part
		if base3Part[0] == '1' {
			scenes = append(scenes, &scene)
		}
	}

	return &models.Video{Scenes: scenes, TargetText: strings.Join(targetText, " "), Base3: base3}, nil
}

type encodeURLError string

func (e encodeURLError) Error() string {
	return string(e)
}

func (e encodeURLError) Public() string {
	return errors.ServerErrMsg
}

func (e encodeURLError) Code() int {
	return 500
}

type decodeURLError string

func (e decodeURLError) Error() string {
	return string(e)
}

func (e decodeURLError) Public() string {
	return errors.ServerErrMsg
}

func (e decodeURLError) Code() int {
	return 500
}
