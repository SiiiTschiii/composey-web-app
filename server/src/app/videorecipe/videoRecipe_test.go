package videorecipe

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"log"
	"math/rand"
	"strings"
	"testing"
)

func TestBase3Parts(t *testing.T) {

	p := toBase3Parts("1021")
	assert.Equal(t, 3, len(p))
	for pos, part := range p {
		switch pos {
		case 0:
			assert.Equal(t, "10", part)
		case 1:
			assert.Equal(t, "2", part)
		case 2:
			assert.Equal(t, "1", part)

		}
	}
}

type videoCandidateMock struct {
	scenes     []*models.Scene
	targetText string
	base3      string
}

func (m *videoCandidateMock) Scenes() []*models.Scene { return m.scenes }
func (m *videoCandidateMock) DocIDs() []string {
	ids := []string{}
	for _, scene := range m.scenes {
		ids = append(ids, scene.DocID)
	}
	return ids
}
func (m *videoCandidateMock) Target() string { return m.targetText }
func (m *videoCandidateMock) Base3() string  { return m.base3 }

var validMock = videoCandidateMock{
	scenes: []*models.Scene{
		{Text: "My name", DocID: "5c532c7f2c7b4e4268219844"},
		{Text: "arni", DocID: "5c532c762c7b4e426821184a"}},
	targetText: "My name is arni",
	base3:      "1021",
}

func TestEncodeDocIDTooShort(t *testing.T) {
	m := videoCandidateMock{}
	copier.Copy(&m, &validMock)

	validID := m.Scenes()[0].DocID
	m.scenes[0].DocID = "invalid_id"

	defer func() {
		m.scenes[0].DocID = validID
	}()

	_, err := Encode(&m)

	assert.EqualError(t, err, "docID too short")

}

func TestEncodeTextLength(t *testing.T) {
	maxString := strings.Repeat("a", 255)
	exceedString := maxString + "b"

	m := videoCandidateMock{}
	copier.Copy(&m, &validMock)

	// max length
	validText := m.Scenes()[1].Text
	defer func() {
		m.scenes[1].Text = validText
	}()

	m.Scenes()[1].Text = maxString
	m.targetText = "My name is " + maxString
	_, err := Encode(&m)
	assert.Equal(t, nil, err)

	// too long
	m.Scenes()[1].Text = exceedString
	m.targetText = "My name is " + exceedString
	_, err = Encode(&m)
	assert.EqualError(t, err, "part text is too long")

}

func TestEncode(t *testing.T) {
	base64String, err := Encode(&validMock)
	assert.Equal(t, nil, err)
	assert.Equal(t, "B015IG5hbWUBXFMsfyx7TkJoIZhEAmlzAARhcm5pAVxTLHYse05CaCEYSg==", base64String)
}

func TestEncodeDecode(t *testing.T) {

	base64String, err := Encode(&validMock)
	assert.Equal(t, nil, err)

	vc, err := Decode(base64String)
	assert.Equal(t, nil, err)

	assert.Equal(t, "My name is arni", vc.TargetText)
	assert.Equal(t, "1021", vc.Base3)
	assert.Equal(t, 2, len(vc.Scenes))
	assert.Equal(t, validMock.Scenes()[0], vc.Scenes[0])
	assert.Equal(t, validMock.Scenes()[1], vc.Scenes[1])
}

func generateRandomNumber(size int, max int) []int {
	randNumber := make([]int, size, size)
	for i := 0; i < size; i++ {
		randNumber[i] = rand.Intn(max)
	}

	return randNumber
}

func TestDecodeRandom(t *testing.T) {
	randInts := generateRandomNumber(50, 2000)
	for _, v := range randInts {
		token := make([]byte, v)
		rand.Read(token)
		randomString := base64.URLEncoding.EncodeToString(token)

		_, err := Decode(randomString)

		assert.Error(t, err, "expected an error")
	}

}

func TestDecodeRandomAfterText(t *testing.T) {
	randInts := generateRandomNumber(50, 2000)
	for _, v := range randInts {
		var b bytes.Buffer
		b.Write([]byte{uint8(7)})
		b.Write([]byte("My name"))

		token := make([]byte, v)
		rand.Read(token)
		b.Write(token)

		randomString := base64.URLEncoding.EncodeToString(b.Bytes())

		_, err := Decode(randomString)

		assert.Error(t, err, "expected an error")
	}
}

func TestDecodeRandomAfterPresentSign(t *testing.T) {
	randInts := generateRandomNumber(50, 2000)
	for _, v := range randInts {
		var b bytes.Buffer
		b.Write([]byte{uint8(7)})
		b.Write([]byte("My name"))
		if v%2 == 0 {
			b.Write([]byte{uint8(1)})
		} else {
			b.Write([]byte{uint8(0)})
		}

		token := make([]byte, v)
		rand.Read(token)
		b.Write(token)

		randomString := base64.URLEncoding.EncodeToString(b.Bytes())

		_, err := Decode(randomString)

		assert.Error(t, err, "expected an error")
	}
}

func TestDecodeRandomAfterDuringDocID(t *testing.T) {
	randInts := generateRandomNumber(50, 2000)
	for _, v := range randInts {
		var b bytes.Buffer
		b.Write([]byte{uint8(7)})
		b.Write([]byte("My name"))
		if v%2 == 0 {
			b.Write([]byte{uint8(1)})
		} else {
			b.Write([]byte{uint8(0)})
		}
		docIDBytes, err := hex.DecodeString("5c53")
		b.Write(docIDBytes)

		token := make([]byte, v)
		rand.Read(token)
		b.Write(token)

		randomString := base64.URLEncoding.EncodeToString(b.Bytes())

		vc, err := Decode(randomString)
		if err == nil {
			log.Printf("%s %s %v", vc.TargetText, vc.Base3, vc.Scenes)
			assert.Error(t, err, "expected an error")
		}
	}
}

// TODO test '+', '/' and '='

func TestStuff(t *testing.T) {

	//assert.Equal(t, "QXJuaWUgY29tcG9zZXMgbWVzc2FnZXMgZnJvbSBtb3ZpZXMu", base64.URLEncoding.EncodeToString([]byte("Arnie composes messages from movies.")))
	//b, err := Decode("BWxldCdzAVxTLIwse05CaCJ06g==")
	//assert.Equal(t, nil, err)
	//assert.Equal(t, "let's party", b.Target())

}

var validMockOneWord = videoCandidateMock{
	[]*models.Scene{{Text: "My", DocID: "5c532c7f2c7b4e4268219844"}},
	"My name is arni",
	"1200",
}

func TestEncodeDecodeOnePart(t *testing.T) {

	base64String, err := Encode(&validMockOneWord)
	assert.Equal(t, nil, err)

	vc, err := Decode(base64String)
	assert.Equal(t, nil, err)

	assert.Equal(t, "My name is arni", vc.TargetText)
	assert.Equal(t, "1200", vc.Base3)
	assert.Equal(t, 1, len(vc.Scenes))
	assert.Equal(t, validMockOneWord.Scenes()[0], vc.Scenes[0])

}

var validMockTwoWords = videoCandidateMock{
	[]*models.Scene{{Text: "let's party", DocID: "5c532c7f2c7b4e4268219844"}},
	"let's party",
	"10",
}

func TestEncodeDecodeTwoWords(t *testing.T) {

	base64String, err := Encode(&validMockTwoWords)
	assert.Equal(t, nil, err)

	vc, err := Decode(base64String)
	assert.Equal(t, nil, err)

	assert.Equal(t, "let's party", vc.TargetText)
	assert.Equal(t, "10", vc.Base3)
	assert.Equal(t, 1, len(vc.Scenes))
	assert.Equal(t, validMockTwoWords.Scenes()[0], vc.Scenes[0])
}
