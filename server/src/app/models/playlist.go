package models

import (
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
)

// PlaylistService is the interface to the service of the Videos model
type PlaylistService interface {
	// Start is starting the Playlist service
	Start() error
	// Stop tears down the Playlist service and terminates the connections to DBs
	Stop() error
	// All returns an array of all available playlists
	All() ([]annotation.Playlist, error)
}

// NewPlaylistService creates a new PlaylistService and returns it as Interfaces
func NewPlaylistService(es config.ElasticConfig) PlaylistService {

	return &playlistService{
		configElastic: es,
	}
}

type playlistService struct {
	elastic       *types.Elastic
	configElastic config.ElasticConfig
}

// Start is starting the Playlist service
func (vs *playlistService) Start() (err error) {

	// select db and collection
	vs.elastic, err = types.NewElastic(vs.configElastic.URL, vs.configElastic.Index)
	if err != nil {
		return err
	}

	return nil
}

// Stop tears down the Playlist service and terminates the connections to DBs
func (vs *playlistService) Stop() error {
	return nil
}

// All returns an array of all available playlists
func (vs *playlistService) All() ([]annotation.Playlist, error) {

	return vs.elastic.Playlists()
}
