package models

import (
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
)

// NewServices constructs the required services for the app
func NewServices(videoDir string, db config.ElasticConfig, workDir string, s3Config config.S3Config, serverMode bool) (*Services, error) {
	return &Services{
		Video:       NewVideoService(videoDir, db, workDir, s3Config, serverMode),
		Composition: NewCompositionService(db),
		Playlist:    NewPlaylistService(db),
	}, nil
}

// Services is the collections of all required sub-services to run the app
type Services struct {
	Video       VideoService
	Composition CompositionService
	Playlist    PlaylistService
}

// Close terminates all the sub-services so as terminating db connections etc
func (s *Services) Close() error {
	err := s.Video.Stop()
	if err != nil {
		return err
	}
	err = s.Composition.Stop()
	if err != nil {
		return err
	}

	err = s.Playlist.Stop()
	if err != nil {
		return err
	}
	return nil
}

// Start starts all sub-services that are associated with the app
func (s *Services) Start() error {
	err := s.Composition.Start()
	if err != nil {
		return err
	}
	err = s.Video.Start()
	if err != nil {
		return err
	}

	err = s.Playlist.Start()
	if err != nil {
		return err
	}

	return nil
}
