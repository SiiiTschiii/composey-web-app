package models

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
	"gitlab.ethz.ch/chgerber/monitor"
	"gitlab.ethz.ch/chgerber/s3helper"
	"gitlab.ethz.ch/chgerber/video"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const (
	// ErrVideoNotFound is returned when the video is not found
	ErrVideoNotFound modelError = "models: video not found"

	// ErrVideoNotUnique is returned when multiple matching videos are found
	ErrVideoNotUnique modelError = "models: multiple matching videos are found"
)

// VideoService is the interface to the service of the Videos model
type VideoService interface {
	Start() error
	Stop() error
	Create(video *Video) error
	CreateOnServer(scene *Scene) error
	//CreateClient(scene *Scene) error
}

// NewVideoService creates a new VideoService and returns it as Interfaces
func NewVideoService(videoDir string, es config.ElasticConfig, workDir string, s3Config config.S3Config, serverMode bool) VideoService {

	return &videoService{
		videoDir:      videoDir,
		workDir:       workDir,
		configElastic: es,
		s3Config:      s3Config,
		serverMode:    serverMode,
	}
}

type videoService struct {
	videoDir      string // needs to be an absolute path
	workDir       string // needs to be an absolute path
	elastic       *types.Elastic
	configElastic config.ElasticConfig
	s3Config      config.S3Config
	serverMode    bool
}

// Start is starting the Video service
func (vs *videoService) Start() (err error) {

	err = os.MkdirAll(vs.videoDir, 0750)
	if err != nil {
		return err
	}

	err = os.MkdirAll(vs.workDir, 0750)
	if err != nil {
		return err
	}

	// select db and collection
	vs.elastic, err = types.NewElastic(vs.configElastic.URL, vs.configElastic.Index)
	if err != nil {
		return err
	}

	return nil
}

// Stop tears down the Video service and terminates the connections to DBs
func (vs *videoService) Stop() error {
	return nil
}

// Video describes a video candidate of a message composition result
type Video struct {
	Scenes     []*Scene
	TargetText string
	Base3      string
}

// Scene describes one part/scene of a video candidate
type Scene struct {
	Text  string // Part text this Scene belongs to.
	DocID string // objectID of the corresponding annotation document
	Doc   types.Part
	Media *Media
}

// Scene implements the interface Input of pkg video
func (scene *Scene) Scene() (src *url.URL, start time.Duration, end time.Duration) {
	src, err := url.Parse(scene.Media.Src)
	if err != nil {
		log.Trace(err)
		return nil, 0, 0
	}

	start = time.Duration(scene.startTime()) * time.Millisecond
	end = time.Duration(scene.endTime()) * time.Millisecond

	return src, start, end
}

func (scene *Scene) startTime() int {

	return scene.Doc.StartTime()
}

func (scene *Scene) endTime() int {

	return scene.Doc.EndTime()
}

// Media describes the media of a scene
type Media struct {
	Src   string `json:"src,required"`
	Type  string `json:"type,required"`
	Start int64  `json:"start",omitempty`
	End   int64  `json:"end",omitempty`
}

type modelError string

func (e modelError) Error() string {
	return string(e)
}

func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] = strings.Title(split[0])
	return strings.Join(split, " ")
}

// Create creates the video given by vc and saves it to disk as filename
func (vs *videoService) Create(video *Video) error {
	defer monitor.Elapsed()()

	for i := range video.Scenes {

		err := vs.GetAnnotation(video.Scenes[i])
		if err != nil {
			return err
		}

		srcURL, err := url.Parse(video.Scenes[i].Doc.Doc.Src)
		if err != nil {
			return err
		}

		if !vs.serverMode && srcURL.Scheme != "s3" {
			log.Trace("Create video on client")
			err = vs.CreateOnClient(video.Scenes[i])
		} else {
			log.Trace("Create video on server")
			err = vs.CreateOnServer(video.Scenes[i])
		}

		if err != nil {
			return err
		}
	}

	return nil
}

// GetAnnotation retrieves the annotaion document with the given id from the doc store
func (vs *videoService) GetAnnotation(scene *Scene) (err error) {

	scene.Doc, err = vs.elastic.RetrievePartByID(scene.DocID, scene.Text)
	if err != nil {
		return err
	}

	return nil
}

func (vs *videoService) CreateOnServer(scene *Scene) error {

	srcURL, err := url.Parse(scene.Doc.Doc.Src)
	if err != nil {
		return err
	}

	// create s3 session for video download
	session := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String(vs.s3Config.Region)},
	}))

	// ffmpeg src
	switch srcURL.Scheme {

	case "s3": // download own content (e.g. s3:///TheBigBangTheory/The.Big.Bang.Theory.S01E01.720p.HDTV.X264-MRSK.mkv)

		f, err := s3helper.DownloadS3Obj(session, vs.s3Config.Bucket, srcURL.Path, vs.workDir)
		if err != nil {
			return err
		}

		// Delete downloaded files at the end of the function
		defer func() {
			os.Remove(f)
		}()

		scene.Media = &Media{Src: "file://" + f}

	case "youtube": // e.g. (youtube://E9MlHA6Np-Q)
		scene.Media = &Media{Src: srcURL.String()}

	case "https": // stream (e.g. https://composey-media.s3.eu-central-1...master.m3u8)
		scene.Media = &Media{Src: srcURL.String()}

	default:
		return fmt.Errorf("'%s' srcURL.Schema does not match any supported cases", srcURL.Scheme)

	}

	// cut the video
	videoFileName := scene.DocID + ".mp4"
	scene.Media.Type = "video/mp4"
	file := filepath.Join(vs.videoDir, videoFileName)
	defer os.Remove(file)
	err = video.Cut(scene, file)
	if err != nil {
		return err
	}

	now := time.Now().AddDate(0, 0, 1)
	// Upload the file to S3.
	s3UpInput := s3manager.UploadInput{
		Bucket:  aws.String("arnie-project-candidates"),
		Key:     aws.String(videoFileName),
		Expires: &now,
	}
	// Upload file to s3
	s3ObjURL, err := s3helper.UploadS3Obj(session, s3UpInput, file)
	log.Tracef("Updated %s", s3ObjURL)
	if err != nil {
		return err
	}

	scene.Media.Src = s3ObjURL

	return nil
}

func (vs *videoService) CreateOnClient(scene *Scene) error {
	srcURL, err := url.Parse(scene.Doc.Doc.Src)
	if err != nil {
		return err
	}

	switch srcURL.Scheme {

	case "youtube": // e.g. (youtube://E9MlHA6Np-Q)
		// youtube video
		scene.Media = &Media{
			Src:  srcURL.Host,
			Type: srcURL.Scheme,
		}
	case "https": // stream (e.g. https://composey-media.s3.eu-central-1...master.m3u8)
		// stream
		scene.Media = &Media{
			Src: srcURL.String(),
		}
	default:

		return fmt.Errorf("'%s' srcURL.Schema does not match any supported cases", srcURL.Scheme)

	}

	scene.Media.Start = int64(scene.startTime())
	scene.Media.End = int64(scene.endTime())

	return nil
}
