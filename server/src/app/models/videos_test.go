package models

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"net/url"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	os.Exit(m.Run())
}

func TestVideosValidRequest(t *testing.T) {
	u, err := url.Parse("s3:///TheBigBangTheory/The.Big.Bang.Theory.S01E01.720p.HDTV.X264-MRSK.mkv")
	assert.Equal(t, nil, err)
	assert.Equal(t, "/TheBigBangTheory/The.Big.Bang.Theory.S01E01.720p.HDTV.X264-MRSK.mkv", u.Path)
}
