package models

import (
	"fmt"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/composition"

	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/materialize"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"

	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/errors"
)

// CompositionService is the interface to the service of the Compositions model
type CompositionService interface {
	Start() error
	Compose(phrase string, playlistID string, reqParameters map[string]interface{}) (Composition, error)
	Stop() error
}

// NewCompositionService creates a new CompositionService and returns it as Interfaces
func NewCompositionService(db config.ElasticConfig) CompositionService {
	return &compositionService{
		configElastic: db,
		defaultWeights: map[string]float64{
			"weightNumParts":   2,
			"weightBalanced":   1,
			"weightSimilarity": 10,
			"weightSplits":     100,
		},
		numberOfCandidates: 100,
	}
}

type compositionService struct {
	configElastic      config.ElasticConfig
	elastic            *types.Elastic
	defaultWeights     map[string]float64
	numberOfCandidates int
}

// Start is starting the Composition service
func (cs *compositionService) Start() (err error) {

	// configure elasticsearch
	cs.elastic, err = types.NewElastic(cs.configElastic.URL, cs.configElastic.Index)
	if err != nil {
		return err
	}

	return nil
}

// Stop tears down the Composition service and terminates the connections to DBs
func (cs *compositionService) Stop() error {
	return nil
}

// Composition is the type that describes one message composition task
type Composition struct {
	TargetText string
	Candidates []types.Candidate
}

// Compose performs a message composition task
func (cs *compositionService) Compose(phrase string, playlistID string, reqParameters map[string]interface{}) (Composition, error) {

	weights, err := cs.setWeigths(reqParameters)
	if err != nil {
		return Composition{}, err
	}

	phrase = util.PreProcess(phrase)
	classes, err := composition.FindParts(phrase, cs.elastic, playlistID)
	if err != nil {
		return Composition{}, err
	}

	candidateClasses := composition.Compose(phrase, classes)

	ref := score.NewReferee(
		&score.NumPart{
			W: weights["weightNumParts"],
		},
		&score.Similarity{
			W:            weights["weightSimilarity"],
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: weights["weightBalanced"],
		},
		&score.Split{
			W: weights["weightSplits"],
		},
	)

	_, candidates, err := materialize.RealizeTop(cs.numberOfCandidates, cs.elastic, ref, candidateClasses, score.Popularity{}, playlistID)
	if err != nil {
		return Composition{}, err
	}

	if len(candidates) == 0 {
		return Composition{}, errors.PublicError("message composition produced zero results")
	}

	return Composition{phrase, candidates}, nil
}

func (cs *compositionService) setWeigths(reqParameters map[string]interface{}) (map[string]float64, error) {

	var weights = make(map[string]float64)

	weightName := "weightNumParts"
	if val, ok := reqParameters[weightName]; ok {

		if valFloat, ok := val.(float64); ok {

			weights[weightName] = valFloat
		} else {

			return nil, fmt.Errorf("parameter %s is not of type float64", weightName)
		}
	} else {

		weights[weightName] = cs.defaultWeights[weightName]
	}

	weightName = "weightBalanced"
	if val, ok := reqParameters[weightName]; ok {
		if valFloat, ok := val.(float64); ok {

			weights[weightName] = valFloat
		} else {

			return nil, fmt.Errorf("parameter %s is not of type float64", weightName)
		}
	} else {

		weights[weightName] = cs.defaultWeights[weightName]
	}

	weightName = "weightSimilarity"
	if val, ok := reqParameters[weightName]; ok {

		if valFloat, ok := val.(float64); ok {

			weights[weightName] = valFloat
		} else {

			return nil, fmt.Errorf("parameter %s is not of type float64", weightName)
		}
	} else {
		weights[weightName] = cs.defaultWeights[weightName]
	}

	weightName = "weightSplits"
	if val, ok := reqParameters[weightName]; ok {

		if valFloat, ok := val.(float64); ok {

			weights[weightName] = valFloat
		} else {
			return nil, fmt.Errorf("parameter %s is not of type float64", weightName)
		}
	} else {

		weights[weightName] = cs.defaultWeights[weightName]
	}

	return weights, nil
}
