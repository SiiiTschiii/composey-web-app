package controllers

import (
	"bytes"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/videorecipe"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

var elasticTestConfig config.ElasticConfig
var elasticTestConfigHLS config.ElasticConfig

type videoCandidateMock struct {
	docIDs     []string
	targetText string
	base3      string
}

func (m *videoCandidateMock) DocIDs() []string { return m.docIDs }
func (m *videoCandidateMock) Target() string   { return m.targetText }
func (m *videoCandidateMock) Base3() string    { return m.base3 }

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	elasticTestConfig = config.ElasticConfig{
		URL:   os.Getenv("ES_URL_TEST"),
		Index: "dev.test",
	}

	os.Exit(m.Run())
}

func isServerErrorResponse(t *testing.T, resp *http.Response) {
	body, _ := ioutil.ReadAll(resp.Body)

	assert.Equal(t, 500, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, `{"error":"something went wrong."}`, string(body), "Body not as expected")
	assert.Equal(t, "500 Internal Server Error", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")
}

func TestVideosEmptyBody(t *testing.T) {
	videosC := NewVideos(models.NewVideoService("", config.ElasticConfig{}, "", config.DefaultS3Config(), false))
	handler := videosC.Create

	req := httptest.NewRequest("", "http://", nil)

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestVideosRandomBody(t *testing.T) {
	videosC := NewVideos(models.NewVideoService("", config.ElasticConfig{}, "", config.DefaultS3Config(), false))
	handler := videosC.Create

	randBytes, err := GenerateRandomBytes(rand.Intn(10000))
	assert.Equal(t, nil, err)

	req, err := http.NewRequest("", "", bytes.NewBuffer(randBytes))
	assert.Equal(t, nil, err)

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestVideosWrongField(t *testing.T) {
	videosC := NewVideos(models.NewVideoService("", config.ElasticConfig{}, "", config.DefaultS3Config(), false))
	handler := videosC.Create

	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	q := req.URL.Query()
	q.Add("w", "C2hlbGxvIHRoZXJlAVxTLHkse05CaCE-jQtsZXQncyBwYXJ0eQFcUyyMLHtOQmgidOoDYnllAVxTLHcse05CaCEfxg==")
	req.URL.RawQuery = q.Encode()

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestVideosRandomField(t *testing.T) {
	videosC := NewVideos(models.NewVideoService("", config.ElasticConfig{}, "", config.DefaultS3Config(), false))
	handler := videosC.Create

	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	randBytes, err := GenerateRandomBytes(rand.Intn(10000))
	assert.Equal(t, nil, err)

	q := req.URL.Query()
	q.Add(string(randBytes), string(randBytes))
	req.URL.RawQuery = q.Encode()

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestVideosMalformedRecipe(t *testing.T) {
	videosC := NewVideos(models.NewVideoService("", config.ElasticConfig{}, "", config.DefaultS3Config(), false))
	handler := videosC.Create

	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	randBytes, err := GenerateRandomBytes(rand.Intn(10000))
	assert.Equal(t, nil, err)

	q := req.URL.Query()
	q.Add("v", string(randBytes))
	req.URL.RawQuery = q.Encode()

	w := httptest.NewRecorder()
	handler(w, req)

	isServerErrorResponse(t, w.Result())
}

func TestVideosEmptyRecipe(t *testing.T) {
	videosC := NewVideos(models.NewVideoService(config.DefaultConfig().VideoDir, config.DefaultElasticConfig(), "", config.DefaultS3Config(), false))
	handler := videosC.Create

	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	q := req.URL.Query()
	q.Add("v", "")
	req.URL.RawQuery = q.Encode()

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestVideosValidRequestClientModeYouTube(t *testing.T) {

	videoCandidateMockYoutube := videoCandidateMock{
		[]string{"5d317cc4ed5f48315275c1a9", "5d317ceded5f48315275caef"},
		"thank you is this",
		"1010",
	}

	videosC := NewVideos(models.NewVideoService(config.DefaultConfig().VideoDir, elasticTestConfig, "/tmp/arnie", config.DefaultS3Config(), false))

	err := videosC.vs.Start()
	assert.Equal(t, nil, err)
	defer videosC.vs.Stop()

	handler := videosC.Create
	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	q := req.URL.Query()
	recipe, err := videorecipe.Encode(&videoCandidateMockYoutube)
	q.Add("v", recipe)
	req.URL.RawQuery = q.Encode()

	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)

	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(body) > 0)
	assert.Equal(t, 200, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, "200 OK", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")

	// validate response json against schema
	// TODO do more validation
	/*
		schema := gojsonschema.NewReferenceLoader("file:////home/christof/.gvm/pkgsets/go1.12/global/src/gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/jsonschema/videos.json")
		document := gojsonschema.NewBytesLoader(body)

		result, err := gojsonschema.Validate(schema, document)
		assert.Equal(t, nil, err)
		if !result.Valid() {
			for _, desc := range result.Errors() {
				fmt.Printf("- %v\n", desc)
			}
			assert.Equal(t, false, true)
		}
	*/
}

func TestVideosValidRequestClientModeS3Streaming(t *testing.T) {

	videoCandidateMockS3streaming := videoCandidateMock{
		[]string{"5d42009849aa8855a79d5882", "5d42009849aa8855a79d58a6"},
		"some poor woman Maybe",
		"1001",
	}

	videosC := NewVideos(models.NewVideoService(config.DefaultConfig().VideoDir, elasticTestConfig, "/tmp/arnie", config.DefaultS3Config(), false))

	err := videosC.vs.Start()
	assert.Equal(t, nil, err)
	defer videosC.vs.Stop()

	handler := videosC.Create
	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	q := req.URL.Query()
	recipe, err := videorecipe.Encode(&videoCandidateMockS3streaming)
	q.Add("v", recipe)
	req.URL.RawQuery = q.Encode()

	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)

	assert.Equal(t, true, strings.Contains(string(body), ".m3u8"))

	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(body) > 0)
	assert.Equal(t, 200, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, "200 OK", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")
}

func TestMongoDBConnectFailure(t *testing.T) {
	videosC := NewVideos(models.NewVideoService(config.DefaultConfig().VideoDir, config.ElasticConfig{}, "/tmp", config.DefaultS3Config(), false))

	// Start should fail as no MongoDB is configured
	err := videosC.vs.Start()

	assert.Error(t, err)

}

func TestMongoDBStop(t *testing.T) {
	videosC := NewVideos(models.NewVideoService(config.DefaultConfig().VideoDir, elasticTestConfig, "/tmp", config.DefaultS3Config(), false))

	defer func() {
		err := videosC.vs.Stop()
		if err != nil {
			assert.Equal(t, nil, err)
		}
	}()

	// Start should fail as no MongoDB is configured
	err := videosC.vs.Start()

	assert.Equal(t, nil, err)
}

func TestFindTBBTepisode(t *testing.T) {
	assert.Equal(t,
		"The.Big.Bang.Theory.S01E16",
		findTBBTepisode("https://composey-media.s3.eu-central-1.amazonaws.com/hls/TheBigBangTheory/The.Big.Bang.Theory.S01E16.720p.HDTV.X264-MRSK/master.m3u8"))
}
