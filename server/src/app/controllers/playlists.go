package controllers

import (
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/views"
	"gitlab.ethz.ch/chgerber/monitor"
	"net/http"
	"os"
	"time"
)

// NewPlaylists creates a new Playlists Controller
func NewPlaylists(ps models.PlaylistService) *Playlists {
	return &Playlists{
		ps: ps,
	}
}

// Playlists is the type that describes the Playlists Controller
type Playlists struct {
	ps models.PlaylistService
}

// PlaylistsRequest is the HTTP request type of the API endpoint GET /videos
type PlaylistsRequest struct {
}

// Create is used to process the query form when a user requests a video candidate
//
// GET /playlists
func (pc *Playlists) Create(w http.ResponseWriter, r *http.Request) {

	//resource monitoring routine
	if os.Getenv("RESOURCE_LOG") == "1" {
		go func() {
			for {
				monitor.LogResourceConsumption(time.Millisecond*1000, "eth0", os.Getenv("DISK_NAME"))
			}
		}()
	}

	resp := views.NewResponse()
	pr := views.PlaylistResponse{}

	// find all playlists
	playlists, err := pc.ps.All()
	if err != nil {
		resp.RenderError(w, err)
		return
	}

	for _, pl := range playlists {

		pr.Playlists = append(pr.Playlists, views.Playlist{Name: pl.Name, ID: pl.ID})
	}

	resp.Render(w, pr)
}
