package controllers

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/views"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type playlistResponse struct {
	Data       views.PlaylistResponse `json:"data,omitempty"`
	Error      interface{}            `json:"error,omitempty"`
	StatusCode int                    `json:"-"`
}

func TestPlaylistValidRequest(t *testing.T) {
	playlistC := NewPlaylists(models.NewPlaylistService(elasticTestConfig))

	err := playlistC.ps.Start()
	assert.Equal(t, nil, err)
	defer playlistC.ps.Stop()

	handler := playlistC.Create
	req, err := http.NewRequest("", "", nil)
	assert.Equal(t, nil, err)

	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()

	body, err := ioutil.ReadAll(resp.Body)
	assert.Equal(t, nil, err)

	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(body) > 0)
	assert.Equal(t, 200, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, "200 OK", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")

	response := playlistResponse{}

	err = json.Unmarshal(body, &response)
	assert.Equal(t, nil, err)
	assert.Equal(t, nil, response.Error)

	assert.Equal(t, "other", response.Data.Playlists[0].Name)
	assert.Equal(t, "test", response.Data.Playlists[1].Name)

}
