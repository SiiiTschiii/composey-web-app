package controllers

import (
	"encoding/json"
	"github.com/gorilla/schema"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/errors"

	"net/http"
	"net/url"
)

func parseJSONRequest(r *http.Request, dst interface{}) error {

	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(dst)
	if err != nil {
		if err.Error() == "EOF" {
			return errors.RequestError("request body is empty")
		}
		return errors.RequestError("request body can't be decoded to json")
	}
	return nil
}

func parseURLParams(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}
	// check that parameters exist
	if len(r.Form) == 0 {
		return errors.RequestError("no url parameters")
	}
	return parseValues(r.Form, dst)
}

func parseValues(values url.Values, dst interface{}) error {
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(dst, values)
	if err != nil {
		return errors.RequestError(err.Error())
	}
	return nil
}
