package controllers

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"

	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
)

func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

func isRequestErrorResponse(t *testing.T, resp *http.Response) {
	body, _ := ioutil.ReadAll(resp.Body)

	assert.Equal(t, 400, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, `{"error":"request is malformed"}`, string(body), "Body not as expected")
	assert.Equal(t, "400 Bad Request", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")
}

func TestCompositionsMalformedQuery(t *testing.T) {
	compositionsC := NewCompositions(models.NewCompositionService(config.ElasticConfig{}))

	handler := compositionsC.Create

	randBytes, err := GenerateRandomBytes(rand.Intn(10000))
	assert.Equal(t, nil, err)

	req := httptest.NewRequest("", "http://", bytes.NewBuffer(randBytes))

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())

}

func TestCompositionsEmptyBody(t *testing.T) {
	compositionsC := NewCompositions(models.NewCompositionService(config.ElasticConfig{}))

	handler := compositionsC.Create
	req := httptest.NewRequest("", "http://", bytes.NewBufferString(""))

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestCompositionsEmptyQuery(t *testing.T) {
	compositionsC := NewCompositions(models.NewCompositionService(config.ElasticConfig{}))

	handler := compositionsC.Create
	req := httptest.NewRequest("", "http://", bytes.NewBufferString(`{"query":""}`))

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestCompositionsLongQuery(t *testing.T) {
	compositionsC := NewCompositions(models.NewCompositionService(config.ElasticConfig{}))

	var buffer bytes.Buffer

	for i := 0; i < maxLengthQuery+1; i++ {
		buffer.WriteString("hello ")
	}

	handler := compositionsC.Create
	req := httptest.NewRequest("", "http://", bytes.NewBufferString(fmt.Sprintf(`{"query":"%s"}`, buffer.String())))

	w := httptest.NewRecorder()
	handler(w, req)

	isRequestErrorResponse(t, w.Result())
}

func TestCompositionsValidQuery(t *testing.T) {

	cs := models.NewCompositionService(elasticTestConfig)
	err := cs.Start()
	assert.Equal(t, nil, err)

	compositionsC := NewCompositions(cs)

	handler := compositionsC.Create
	req := httptest.NewRequest("", "http://", bytes.NewBufferString(`{"query":"hello","playlistID":"test"}`))

	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)

	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(body) > 0)
	assert.Equal(t, 200, resp.StatusCode, "Statuscode not as expected")
	assert.Equal(t, 1, len(resp.Header), "Number of headers not as expected")
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"), "Content-Type header not as expected")
	assert.Equal(t, "200 OK", resp.Status, "Status not as expected")
	assert.Equal(t, false, resp.Close, "Connection close indicator bool for client is not as expected")
	assert.Equal(t, int64(-1), resp.ContentLength, "ConntentLength is not as expected")
	assert.Equal(t, false, resp.Uncompressed, "")

	// validate response json against schema
	// TODO needs absolute path or url
	/*
		schema := gojsonschema.NewReferenceLoader("file:////home/christof/.gvm/pkgsets/go1.12/global/src/gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/jsonschema/compositions.json")
		document := gojsonschema.NewBytesLoader(body)

		result, err := gojsonschema.Validate(schema, document)
		assert.Equal(t, nil, err)
		if !result.Valid() {
			for _, err := range result.Errors() {
				assert.Equal(t, nil, err)
			}
		}
	*/
}

func TestParseRequest(t *testing.T) {

	var reqBody = `
	{
		"query": "hello there",
		"parameters": {
			"weightNumParts": 2,
			"weightBalanced": 1,
			"weightSimilarity": 10,
			"weightSplits": 100
			
		}
	}`

	req := httptest.NewRequest("", "http://", bytes.NewBufferString(reqBody))

	// parse request
	var reqData CompositionsRequest

	err := parseJSONRequest(req, &reqData)
	assert.Equal(t, nil, err)

	assert.Equal(t, "hello there", reqData.Query)

	assert.Equal(t, 4, len(reqData.Parameters))

	val, ok := reqData.Parameters["weightNumParts"]
	assert.Equal(t, true, ok)
	valInt, ok := val.(float64)
	assert.Equal(t, true, ok)
	assert.Equal(t, 2.0, valInt)

	val, ok = reqData.Parameters["weightSplits"]
	assert.Equal(t, true, ok)
	valInt, ok = val.(float64)
	assert.Equal(t, true, ok)
	assert.Equal(t, 100.0, valInt)

}

func TestInvalidWeigths(t *testing.T) {

	var reqBody = `
	{
		"query": "hello there",
		"parameters": {
			"weightNumParts": 2,
			"weightSimilarity": 10,
			"weightSplits": "something"
			
		}
	}`

	cs := models.NewCompositionService(elasticTestConfig)
	err := cs.Start()
	assert.Equal(t, nil, err)

	compositionsC := NewCompositions(cs)

	handler := compositionsC.Create

	req := httptest.NewRequest("", "http://", bytes.NewBufferString(reqBody))

	w := httptest.NewRecorder()
	handler(w, req)

	isServerErrorResponse(t, w.Result())

}
