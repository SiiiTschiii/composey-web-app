package controllers

import (
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/videorecipe"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/views"
	"gitlab.ethz.ch/chgerber/monitor"
	"net/http"
	"os"
	"path"
	"regexp"
	"time"
)

// NewVideos creates a new Videos Controller
func NewVideos(vs models.VideoService) *Videos {
	return &Videos{
		vs: vs,
	}
}

// Videos is the type that describes the Videos Controller
type Videos struct {
	vs models.VideoService
}

// VideosRequest is the HTTP request type of the API endpoint GET /videos
type VideosRequest struct {
	Recipe string `schema:"v,required"`
}

// Create is used to process the query form when a user requests a video candidate
//
// GET /videos
func (vc *Videos) Create(w http.ResponseWriter, r *http.Request) {

	//resource monitoring routine
	if os.Getenv("RESOURCE_LOG") == "1" {
		go func() {
			for {
				monitor.LogResourceConsumption(time.Millisecond*1000, "eth0", os.Getenv("DISK_NAME"))
			}
		}()
	}

	resp := views.NewResponse()
	vr := views.VideosResponse{}

	// parse request
	var vReq VideosRequest

	err := parseURLParams(r, &vReq)
	if err != nil {
		resp.RenderError(w, err)
		return
	}

	vr.Video.ID = vReq.Recipe

	// decode recipe
	video, err := videorecipe.Decode(vReq.Recipe)
	if err != nil {
		resp.RenderError(w, err)
		return
	}

	vr.Video.Query = video.TargetText

	// create video
	err = vc.vs.Create(video)
	if err != nil {
		resp.RenderError(w, err)
		return
	}

	for i := range video.Scenes {

		vr.Video.Media = append(vr.Video.Media, *video.Scenes[i].Media)

		p := views.Part{}
		p.Text = video.Scenes[i].Text

		if video.Scenes[i].Doc.Split {
			p.SubText = video.Scenes[i].Doc.Highlight
		} else {
			p.SubText = "<em>" + video.Scenes[i].Doc.ExtractHighlight() + "</em>"
		}

		// Dirty solution to extract the episode of from TBBT url
		movieName := findTBBTepisode(video.Scenes[i].Doc.Doc.Src)

		if movieName == "" {
			p.Movie = path.Base(video.Scenes[i].Doc.Doc.Src)
		} else {
			p.Movie = movieName
		}

		p.Start = video.Scenes[i].Media.Start
		p.End = video.Scenes[i].Media.End

		vr.Video.Info.Parts = append(vr.Video.Info.Parts, p)

	}

	resp.Render(w, vr)
}

func findTBBTepisode(src string) string {
	var validID = regexp.MustCompile(`The.Big.Bang.Theory.S[0-9]*E[0-9]*`)

	return validID.FindString(src)
}
