package controllers

import (
	"fmt"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/errors"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/videorecipe"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/views"
	"strings"

	"net/http"
)

const maxLengthQuery = 10

// NewCompositions creates a new Compositions Controller
func NewCompositions(cs models.CompositionService) *Compositions {
	return &Compositions{
		cs: cs,
	}
}

// Compositions is the type that describes the Compositions Controller
type Compositions struct {
	cs models.CompositionService
}

// CompositionsRequest is the HTTP request type of the API endpoint POST /compositions
type CompositionsRequest struct {
	Query      string                 `json:"query,required"`
	Parameters map[string]interface{} `json:"parameters"`
	PlaylistID string                 `json:"playlistID,required"`
}

// validate and parse the input http request of POST /compositions
func validate(r *http.Request) (data *CompositionsRequest, err error) {

	err = parseJSONRequest(r, &data)
	if err != nil {
		return nil, err
	}

	// min length of composition query
	if len(data.Query) == 0 {
		return nil, errors.RequestError("query field in request is empty")
	}

	// max length of composition query
	if len(strings.Fields(data.Query)) > maxLengthQuery {
		return nil, errors.RequestError(fmt.Sprintf("query text exceeds max length of %d terms", maxLengthQuery))
	}

	return data, nil
}

// Create is used to process the request form when a user
// submits a message composition request.
//
// POST /compositions
func (cc *Compositions) Create(w http.ResponseWriter, r *http.Request) {

	resp := views.NewResponse()

	// parse and validate request
	reqData, err := validate(r)
	if err != nil {
		resp.RenderError(w, err)
		return
	}

	// compose message
	c, err := cc.cs.Compose(reqData.Query, reqData.PlaylistID, reqData.Parameters)
	if err != nil {
		resp.RenderError(w, err)
		return
	}

	// Transform candidates into video recipe strings
	var recipes []string
	for _, candidate := range c.Candidates {
		recipe, err := videorecipe.Encode(&candidate)
		if err != nil {
			resp.RenderError(w, err)
			return
		}
		recipes = append(recipes, recipe)
	}

	resp.Render(w, views.CompositionsResponse{Candidates: recipes})

}
