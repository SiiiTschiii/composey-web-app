package app

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/config"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/controllers"
	"gitlab.ethz.ch/chgerber/composey-web-app/server/src/app/models"
	"gitlab.ethz.ch/chgerber/monitor"
	"net/http"
	"path"
)

// App is the type that represents the web application
type App struct {
	Services *models.Services
	Router   *mux.Router
	Config   config.Config
}

func (a *App) initializeRoutes() {
	// API
	// controllers
	compositionsC := controllers.NewCompositions(a.Services.Composition)
	//a.Router.HandleFunc("/", compositionsC.New).Methods("GET")
	a.Router.HandleFunc("/compositions", monitor.Timed(compositionsC.Create)).Methods("POST")

	videosC := controllers.NewVideos(a.Services.Video)
	a.Router.HandleFunc("/videos", monitor.Timed(videosC.Create)).Methods("GET")

	playlistC := controllers.NewPlaylists(a.Services.Playlist)
	a.Router.HandleFunc("/playlists", monitor.Timed(playlistC.Create)).Methods("GET")

	// Static stuff
	staticHandler := http.FileServer(http.Dir(path.Join(a.Config.AppFiles, "static")))
	a.Router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", staticHandler))

	buildHandler := http.FileServer(http.Dir(a.Config.AppFiles))
	a.Router.PathPrefix("/").Handler(buildHandler)
}

// Initialize the app
// configures and starts the services and router.
func (a *App) Initialize(boolPtr *bool, configFilePtr *string) {
	flag.Parse()
	a.Config = config.LoadConfig(*boolPtr, *configFilePtr)

	var err error
	a.Services, err = models.NewServices(a.Config.VideoDir, a.Config.AnnotationStore, a.Config.WorkDir, a.Config.S3, a.Config.ServerMode)
	if err != nil {
		panic(err)
	}

	err = a.Services.Start()
	if err != nil {
		panic(err)
	}

	a.Router = mux.NewRouter()

	a.initializeRoutes()
}

// Run starts the app.
func (a *App) Run() {
	defer a.Services.Close()
	log.Printf("Starting the server on :%d", a.Config.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", a.Config.Port), a.Router))
}
