package errors


const (
	// ServerErrMsg is sent when any random error is encountered by our backend.
	ServerErrMsg = "something went wrong."
	// RequestErrMsg is used as error message when a malformed http request is detected
	RequestErrMsg = "request is malformed"
)

// HTTPError serves as control interface to decide what error messages to be exposed to the client
type HTTPError interface {
	error
	Public() string
	Code() int
}

// RequestError is the error type for cases when a malformed http request is detected
type RequestError string

func (e RequestError) Error() string {
	return string(e)
}

// Public returns the error message for user access
func (e RequestError) Public() string {
	return RequestErrMsg
}

// Code returns the http status-code that is associated with the error
func (e RequestError) Code() int {
	return 400
}

// ServerError is the error type for cases when the server has detected an error
type ServerError string


func (e ServerError) Error() string {
	return string(e)
}

// Public returns the error message for user access
func (e ServerError) Public() string {
	return ServerErrMsg
}

// Code returns the http status-code that is associated with the error
func (e ServerError) Code() int {
	return 500
}

// PublicError is the error type for cases when the server has detected an error
type PublicError string

// PublicError returns the private error message as public error message
func (e PublicError) Error() string {
	return string(e)
}

// Public returns the error message for public access. In this case Public returns the direct error message.
func (e PublicError) Public() string {
	return string(e)
}

// Code returns the http status-code that is associated with the error
func (e PublicError) Code() int {
	return 500
}