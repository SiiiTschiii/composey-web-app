module gitlab.ethz.ch/chgerber/composey-web-app

go 1.13

require (
	cloud.google.com/go v0.49.0 // indirect
	github.com/aws/aws-sdk-go v1.25.49
	github.com/golang/groupcache v0.0.0-20191027212112-611e8accdfc9 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.1.0
	github.com/jinzhu/copier v0.0.0-20180308034124-7e38e58719c3
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	gitlab.ethz.ch/chgerber/MessageComposition v0.4.5
	gitlab.ethz.ch/chgerber/annotation/v2 v2.2.0
	gitlab.ethz.ch/chgerber/monitor v0.0.0-20190527191251-2bb9dd731340
	gitlab.ethz.ch/chgerber/s3helper v0.0.0-20190527212420-8196b9b9a04f
	gitlab.ethz.ch/chgerber/video v0.0.0-20190527150454-f0fa080cc6fa
	gitlab.ethz.ch/chgerber/youtubedl v1.2.0
	go.mongodb.org/mongo-driver v1.1.3
	go.opencensus.io v0.22.2 // indirect
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/genproto v0.0.0-20191206224255-0243a4be9c8f // indirect
	google.golang.org/grpc v1.25.1 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect

)
